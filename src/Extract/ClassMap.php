<?php

declare(strict_types=1);

namespace Extract;

/**
 * Class which returns the class map definition
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get(): array
    {
        return [
            'extract' => '\\Extract\\StructType\\Extract',
            'extractParameter' => '\\Extract\\StructType\\ExtractParameter',
            'mimeDocument' => '\\Extract\\StructType\\MimeDocument',
            'APIException' => '\\Extract\\StructType\\APIException',
            'errorDetail' => '\\Extract\\StructType\\ErrorDetail',
            'availableExtract' => '\\Extract\\StructType\\AvailableExtract',
            'availableExtractParameter' => '\\Extract\\StructType\\AvailableExtractParameter',
            'apiPersonKey' => '\\Extract\\StructType\\ApiPersonKey',
            'compositeKey' => '\\Extract\\StructType\\CompositeKey',
            'availableExtractArray' => '\\Extract\\ArrayType\\AvailableExtractArray',
            'apiPersonKeyArray' => '\\Extract\\ArrayType\\ApiPersonKeyArray',
        ];
    }
}
