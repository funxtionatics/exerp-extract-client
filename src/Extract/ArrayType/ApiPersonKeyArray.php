<?php

declare(strict_types=1);

namespace Extract\ArrayType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for apiPersonKeyArray ArrayType
 * Meta information extracted from the WSDL
 * - final: #all
 * @subpackage Arrays
 */
class ApiPersonKeyArray extends AbstractStructArrayBase
{
    /**
     * The item
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Extract\StructType\ApiPersonKey[]
     */
    protected array $item = [];
    /**
     * Constructor method for apiPersonKeyArray
     * @uses ApiPersonKeyArray::setItem()
     * @param \Extract\StructType\ApiPersonKey[] $item
     */
    public function __construct(array $item = [])
    {
        $this
            ->setItem($item);
    }
    /**
     * Get item value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Extract\StructType\ApiPersonKey[]
     */
    public function getItem(): ?array
    {
        return isset($this->item) ? $this->item : null;
    }
    /**
     * This method is responsible for validating the values passed to the setItem method
     * This method is willingly generated in order to preserve the one-line inline validation within the setItem method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateItemForArrayConstraintsFromSetItem(array $values = []): string
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $apiPersonKeyArrayItemItem) {
            // validation for constraint: itemType
            if (!$apiPersonKeyArrayItemItem instanceof \Extract\StructType\ApiPersonKey) {
                $invalidValues[] = is_object($apiPersonKeyArrayItemItem) ? get_class($apiPersonKeyArrayItemItem) : sprintf('%s(%s)', gettype($apiPersonKeyArrayItemItem), var_export($apiPersonKeyArrayItemItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The item property can only contain items of type \Extract\StructType\ApiPersonKey, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set item value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Extract\StructType\ApiPersonKey[] $item
     * @return \Extract\ArrayType\ApiPersonKeyArray
     */
    public function setItem(array $item = []): self
    {
        // validation for constraint: array
        if ('' !== ($itemArrayErrorMessage = self::validateItemForArrayConstraintsFromSetItem($item))) {
            throw new InvalidArgumentException($itemArrayErrorMessage, __LINE__);
        }
        if (is_null($item) || (is_array($item) && empty($item))) {
            unset($this->item);
        } else {
            $this->item = $item;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \Extract\StructType\ApiPersonKey|null
     */
    public function current(): ?\Extract\StructType\ApiPersonKey
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \Extract\StructType\ApiPersonKey|null
     */
    public function item($index): ?\Extract\StructType\ApiPersonKey
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \Extract\StructType\ApiPersonKey|null
     */
    public function first(): ?\Extract\StructType\ApiPersonKey
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \Extract\StructType\ApiPersonKey|null
     */
    public function last(): ?\Extract\StructType\ApiPersonKey
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \Extract\StructType\ApiPersonKey|null
     */
    public function offsetGet($offset): ?\Extract\StructType\ApiPersonKey
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \Extract\StructType\ApiPersonKey $item
     * @return \Extract\ArrayType\ApiPersonKeyArray
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Extract\StructType\ApiPersonKey) {
            throw new InvalidArgumentException(sprintf('The item property can only contain items of type \Extract\StructType\ApiPersonKey, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string item
     */
    public function getAttributeName(): string
    {
        return 'item';
    }
}
