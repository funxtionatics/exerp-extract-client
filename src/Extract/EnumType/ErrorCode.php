<?php

declare(strict_types=1);

namespace Extract\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for errorCode EnumType
 * @subpackage Enumerations
 */
class ErrorCode extends AbstractStructEnumBase
{
    /**
     * Constant for value 'UNKNOWN_ERROR'
     * @return string 'UNKNOWN_ERROR'
     */
    const VALUE_UNKNOWN_ERROR = 'UNKNOWN_ERROR';
    /**
     * Constant for value 'CREATE_EXCEPTION'
     * @return string 'CREATE_EXCEPTION'
     */
    const VALUE_CREATE_EXCEPTION = 'CREATE_EXCEPTION';
    /**
     * Constant for value 'REMOVE_EXCEPTION'
     * @return string 'REMOVE_EXCEPTION'
     */
    const VALUE_REMOVE_EXCEPTION = 'REMOVE_EXCEPTION';
    /**
     * Constant for value 'SECURITY'
     * @return string 'SECURITY'
     */
    const VALUE_SECURITY = 'SECURITY';
    /**
     * Constant for value 'SECURITY_ROLE'
     * @return string 'SECURITY_ROLE'
     */
    const VALUE_SECURITY_ROLE = 'SECURITY_ROLE';
    /**
     * Constant for value 'ARGUMENT_IS_NULL'
     * @return string 'ARGUMENT_IS_NULL'
     */
    const VALUE_ARGUMENT_IS_NULL = 'ARGUMENT_IS_NULL';
    /**
     * Constant for value 'ILLEGAL_ARGUMENT'
     * @return string 'ILLEGAL_ARGUMENT'
     */
    const VALUE_ILLEGAL_ARGUMENT = 'ILLEGAL_ARGUMENT';
    /**
     * Constant for value 'ILLEGAL_STATE'
     * @return string 'ILLEGAL_STATE'
     */
    const VALUE_ILLEGAL_STATE = 'ILLEGAL_STATE';
    /**
     * Constant for value 'PARSE_ERROR'
     * @return string 'PARSE_ERROR'
     */
    const VALUE_PARSE_ERROR = 'PARSE_ERROR';
    /**
     * Constant for value 'ENTITY_NOT_FOUND'
     * @return string 'ENTITY_NOT_FOUND'
     */
    const VALUE_ENTITY_NOT_FOUND = 'ENTITY_NOT_FOUND';
    /**
     * Constant for value 'CONFLICTING_CHANGE'
     * @return string 'CONFLICTING_CHANGE'
     */
    const VALUE_CONFLICTING_CHANGE = 'CONFLICTING_CHANGE';
    /**
     * Constant for value 'INVALID_ZIPCODE'
     * @return string 'INVALID_ZIPCODE'
     */
    const VALUE_INVALID_ZIPCODE = 'INVALID_ZIPCODE';
    /**
     * Constant for value 'INVALID_SOCIAL_SECURITY_NUMBER'
     * @return string 'INVALID_SOCIAL_SECURITY_NUMBER'
     */
    const VALUE_INVALID_SOCIAL_SECURITY_NUMBER = 'INVALID_SOCIAL_SECURITY_NUMBER';
    /**
     * Constant for value 'INVALID_COMPANYID'
     * @return string 'INVALID_COMPANYID'
     */
    const VALUE_INVALID_COMPANYID = 'INVALID_COMPANYID';
    /**
     * Constant for value 'INVALID_PHONE_NUMBER'
     * @return string 'INVALID_PHONE_NUMBER'
     */
    const VALUE_INVALID_PHONE_NUMBER = 'INVALID_PHONE_NUMBER';
    /**
     * Constant for value 'TOO_EARLY'
     * @return string 'TOO_EARLY'
     */
    const VALUE_TOO_EARLY = 'TOO_EARLY';
    /**
     * Constant for value 'TOO_LATE'
     * @return string 'TOO_LATE'
     */
    const VALUE_TOO_LATE = 'TOO_LATE';
    /**
     * Constant for value 'CLIENT'
     * @return string 'CLIENT'
     */
    const VALUE_CLIENT = 'CLIENT';
    /**
     * Constant for value 'ENTITY_IDENTIFIER_STATUS'
     * @return string 'ENTITY_IDENTIFIER_STATUS'
     */
    const VALUE_ENTITY_IDENTIFIER_STATUS = 'ENTITY_IDENTIFIER_STATUS';
    /**
     * Constant for value 'DUPLICATE_PERSON'
     * @return string 'DUPLICATE_PERSON'
     */
    const VALUE_DUPLICATE_PERSON = 'DUPLICATE_PERSON';
    /**
     * Constant for value 'DUPLICATE_EXTERNAL_ID'
     * @return string 'DUPLICATE_EXTERNAL_ID'
     */
    const VALUE_DUPLICATE_EXTERNAL_ID = 'DUPLICATE_EXTERNAL_ID';
    /**
     * Constant for value 'FRIEND_RESTRICTION'
     * @return string 'FRIEND_RESTRICTION'
     */
    const VALUE_FRIEND_RESTRICTION = 'FRIEND_RESTRICTION';
    /**
     * Constant for value 'EMPLOYEE_ALREADY_EXIST'
     * @return string 'EMPLOYEE_ALREADY_EXIST'
     */
    const VALUE_EMPLOYEE_ALREADY_EXIST = 'EMPLOYEE_ALREADY_EXIST';
    /**
     * Constant for value 'DUPLICATE_COMPANY'
     * @return string 'DUPLICATE_COMPANY'
     */
    const VALUE_DUPLICATE_COMPANY = 'DUPLICATE_COMPANY';
    /**
     * Constant for value 'EXPENSIVE_SEARCH'
     * @return string 'EXPENSIVE_SEARCH'
     */
    const VALUE_EXPENSIVE_SEARCH = 'EXPENSIVE_SEARCH';
    /**
     * Constant for value 'MULTIPLE_PERSONS_FOUND'
     * @return string 'MULTIPLE_PERSONS_FOUND'
     */
    const VALUE_MULTIPLE_PERSONS_FOUND = 'MULTIPLE_PERSONS_FOUND';
    /**
     * Constant for value 'NOT_DELIVERABLE'
     * @return string 'NOT_DELIVERABLE'
     */
    const VALUE_NOT_DELIVERABLE = 'NOT_DELIVERABLE';
    /**
     * Constant for value 'CIRCULAR_REFERENCE'
     * @return string 'CIRCULAR_REFERENCE'
     */
    const VALUE_CIRCULAR_REFERENCE = 'CIRCULAR_REFERENCE';
    /**
     * Constant for value 'REFERENCE'
     * @return string 'REFERENCE'
     */
    const VALUE_REFERENCE = 'REFERENCE';
    /**
     * Constant for value 'PERSON_NOT_EDITABLE'
     * @return string 'PERSON_NOT_EDITABLE'
     */
    const VALUE_PERSON_NOT_EDITABLE = 'PERSON_NOT_EDITABLE';
    /**
     * Constant for value 'PERSON_TRANSFER'
     * @return string 'PERSON_TRANSFER'
     */
    const VALUE_PERSON_TRANSFER = 'PERSON_TRANSFER';
    /**
     * Constant for value 'NO_CLEARING_HOUSE_FOUND'
     * @return string 'NO_CLEARING_HOUSE_FOUND'
     */
    const VALUE_NO_CLEARING_HOUSE_FOUND = 'NO_CLEARING_HOUSE_FOUND';
    /**
     * Constant for value 'ACCOUNT_RECEIVABLE_IS_BLOCKED'
     * @return string 'ACCOUNT_RECEIVABLE_IS_BLOCKED'
     */
    const VALUE_ACCOUNT_RECEIVABLE_IS_BLOCKED = 'ACCOUNT_RECEIVABLE_IS_BLOCKED';
    /**
     * Constant for value 'BANK_ACCOUNT_IS_BLOCKED'
     * @return string 'BANK_ACCOUNT_IS_BLOCKED'
     */
    const VALUE_BANK_ACCOUNT_IS_BLOCKED = 'BANK_ACCOUNT_IS_BLOCKED';
    /**
     * Constant for value 'DEBIT_MAX'
     * @return string 'DEBIT_MAX'
     */
    const VALUE_DEBIT_MAX = 'DEBIT_MAX';
    /**
     * Constant for value 'INVALID_PAYMENT_AGREEMENT'
     * @return string 'INVALID_PAYMENT_AGREEMENT'
     */
    const VALUE_INVALID_PAYMENT_AGREEMENT = 'INVALID_PAYMENT_AGREEMENT';
    /**
     * Constant for value 'INVALID_BANK_INFO'
     * @return string 'INVALID_BANK_INFO'
     */
    const VALUE_INVALID_BANK_INFO = 'INVALID_BANK_INFO';
    /**
     * Constant for value 'NO_CLEARING_HOUSE_CREDITOR_FOUND'
     * @return string 'NO_CLEARING_HOUSE_CREDITOR_FOUND'
     */
    const VALUE_NO_CLEARING_HOUSE_CREDITOR_FOUND = 'NO_CLEARING_HOUSE_CREDITOR_FOUND';
    /**
     * Constant for value 'PARTICIPATION_TIME_INTERVAL_OVERLAP'
     * @return string 'PARTICIPATION_TIME_INTERVAL_OVERLAP'
     */
    const VALUE_PARTICIPATION_TIME_INTERVAL_OVERLAP = 'PARTICIPATION_TIME_INTERVAL_OVERLAP';
    /**
     * Constant for value 'PARTICIPATION_PERSON_BLACKLISTED'
     * @return string 'PARTICIPATION_PERSON_BLACKLISTED'
     */
    const VALUE_PARTICIPATION_PERSON_BLACKLISTED = 'PARTICIPATION_PERSON_BLACKLISTED';
    /**
     * Constant for value 'PARTICIPATION_PERSON_BLOCKED'
     * @return string 'PARTICIPATION_PERSON_BLOCKED'
     */
    const VALUE_PARTICIPATION_PERSON_BLOCKED = 'PARTICIPATION_PERSON_BLOCKED';
    /**
     * Constant for value 'PARTICIPATION_PERSON_DOUBLE_PARTICIPATION'
     * @return string 'PARTICIPATION_PERSON_DOUBLE_PARTICIPATION'
     */
    const VALUE_PARTICIPATION_PERSON_DOUBLE_PARTICIPATION = 'PARTICIPATION_PERSON_DOUBLE_PARTICIPATION';
    /**
     * Constant for value 'BOOKING_WAITING_LIST_FULL'
     * @return string 'BOOKING_WAITING_LIST_FULL'
     */
    const VALUE_BOOKING_WAITING_LIST_FULL = 'BOOKING_WAITING_LIST_FULL';
    /**
     * Constant for value 'BOOKING_OUTSIDE_TIME_INTERVAL'
     * @return string 'BOOKING_OUTSIDE_TIME_INTERVAL'
     */
    const VALUE_BOOKING_OUTSIDE_TIME_INTERVAL = 'BOOKING_OUTSIDE_TIME_INTERVAL';
    /**
     * Constant for value 'BOOKING_RESTRICTION'
     * @return string 'BOOKING_RESTRICTION'
     */
    const VALUE_BOOKING_RESTRICTION = 'BOOKING_RESTRICTION';
    /**
     * Constant for value 'OUTSIDE_BOOKING_WINDOW_EXCEPTION'
     * @return string 'OUTSIDE_BOOKING_WINDOW_EXCEPTION'
     */
    const VALUE_OUTSIDE_BOOKING_WINDOW_EXCEPTION = 'OUTSIDE_BOOKING_WINDOW_EXCEPTION';
    /**
     * Constant for value 'CUSTOMER_CANCEL_OUTSIDE_TIME_INTERVAL'
     * @return string 'CUSTOMER_CANCEL_OUTSIDE_TIME_INTERVAL'
     */
    const VALUE_CUSTOMER_CANCEL_OUTSIDE_TIME_INTERVAL = 'CUSTOMER_CANCEL_OUTSIDE_TIME_INTERVAL';
    /**
     * Constant for value 'CENTER_CANCEL_OUTSIDE_TIME_INTERVAL'
     * @return string 'CENTER_CANCEL_OUTSIDE_TIME_INTERVAL'
     */
    const VALUE_CENTER_CANCEL_OUTSIDE_TIME_INTERVAL = 'CENTER_CANCEL_OUTSIDE_TIME_INTERVAL';
    /**
     * Constant for value 'BOOKING_COLLECTION_NOT_PAID_FOR'
     * @return string 'BOOKING_COLLECTION_NOT_PAID_FOR'
     */
    const VALUE_BOOKING_COLLECTION_NOT_PAID_FOR = 'BOOKING_COLLECTION_NOT_PAID_FOR';
    /**
     * Constant for value 'NO_PRIVILEGE_FOR_PARTICIPATION'
     * @return string 'NO_PRIVILEGE_FOR_PARTICIPATION'
     */
    const VALUE_NO_PRIVILEGE_FOR_PARTICIPATION = 'NO_PRIVILEGE_FOR_PARTICIPATION';
    /**
     * Constant for value 'ALL_PARTICIPATIONS_TAKEN'
     * @return string 'ALL_PARTICIPATIONS_TAKEN'
     */
    const VALUE_ALL_PARTICIPATIONS_TAKEN = 'ALL_PARTICIPATIONS_TAKEN';
    /**
     * Constant for value 'SHOWUP_OUTSIDE_TIME_INTERVAL'
     * @return string 'SHOWUP_OUTSIDE_TIME_INTERVAL'
     */
    const VALUE_SHOWUP_OUTSIDE_TIME_INTERVAL = 'SHOWUP_OUTSIDE_TIME_INTERVAL';
    /**
     * Constant for value 'ILLEGAL_BOOKING_STATE_FOR_CANCELLATION'
     * @return string 'ILLEGAL_BOOKING_STATE_FOR_CANCELLATION'
     */
    const VALUE_ILLEGAL_BOOKING_STATE_FOR_CANCELLATION = 'ILLEGAL_BOOKING_STATE_FOR_CANCELLATION';
    /**
     * Constant for value 'TOO_EARLY_TO_BOOK_PARTICIPANT'
     * @return string 'TOO_EARLY_TO_BOOK_PARTICIPANT'
     */
    const VALUE_TOO_EARLY_TO_BOOK_PARTICIPANT = 'TOO_EARLY_TO_BOOK_PARTICIPANT';
    /**
     * Constant for value 'TOO_EARLY_TO_SHOWUP'
     * @return string 'TOO_EARLY_TO_SHOWUP'
     */
    const VALUE_TOO_EARLY_TO_SHOWUP = 'TOO_EARLY_TO_SHOWUP';
    /**
     * Constant for value 'TOO_LATE_TO_SHOWUP'
     * @return string 'TOO_LATE_TO_SHOWUP'
     */
    const VALUE_TOO_LATE_TO_SHOWUP = 'TOO_LATE_TO_SHOWUP';
    /**
     * Constant for value 'PARTICIPANT_TOO_MANY_OPEN_BOOKINGS'
     * @return string 'PARTICIPANT_TOO_MANY_OPEN_BOOKINGS'
     */
    const VALUE_PARTICIPANT_TOO_MANY_OPEN_BOOKINGS = 'PARTICIPANT_TOO_MANY_OPEN_BOOKINGS';
    /**
     * Constant for value 'RESOURCE_OCCUPIED'
     * @return string 'RESOURCE_OCCUPIED'
     */
    const VALUE_RESOURCE_OCCUPIED = 'RESOURCE_OCCUPIED';
    /**
     * Constant for value 'RESOURCE_START_TIME_RESTRICTION'
     * @return string 'RESOURCE_START_TIME_RESTRICTION'
     */
    const VALUE_RESOURCE_START_TIME_RESTRICTION = 'RESOURCE_START_TIME_RESTRICTION';
    /**
     * Constant for value 'RESOURCE_TIME_RESTRICTION'
     * @return string 'RESOURCE_TIME_RESTRICTION'
     */
    const VALUE_RESOURCE_TIME_RESTRICTION = 'RESOURCE_TIME_RESTRICTION';
    /**
     * Constant for value 'ACTIVITY_STATE_INVALID'
     * @return string 'ACTIVITY_STATE_INVALID'
     */
    const VALUE_ACTIVITY_STATE_INVALID = 'ACTIVITY_STATE_INVALID';
    /**
     * Constant for value 'NO_PARENT_BOOKING_FOUND'
     * @return string 'NO_PARENT_BOOKING_FOUND'
     */
    const VALUE_NO_PARENT_BOOKING_FOUND = 'NO_PARENT_BOOKING_FOUND';
    /**
     * Constant for value 'STAFF_BLACKLISTED'
     * @return string 'STAFF_BLACKLISTED'
     */
    const VALUE_STAFF_BLACKLISTED = 'STAFF_BLACKLISTED';
    /**
     * Constant for value 'STAFF_OCCUPIED'
     * @return string 'STAFF_OCCUPIED'
     */
    const VALUE_STAFF_OCCUPIED = 'STAFF_OCCUPIED';
    /**
     * Constant for value 'TOO_MANY_SUB_STAFF_USAGES'
     * @return string 'TOO_MANY_SUB_STAFF_USAGES'
     */
    const VALUE_TOO_MANY_SUB_STAFF_USAGES = 'TOO_MANY_SUB_STAFF_USAGES';
    /**
     * Constant for value 'PARTICIPANT_ALREADY_BOOKED'
     * @return string 'PARTICIPANT_ALREADY_BOOKED'
     */
    const VALUE_PARTICIPANT_ALREADY_BOOKED = 'PARTICIPANT_ALREADY_BOOKED';
    /**
     * Constant for value 'PARTICIPANT_ALREADY_SHOWEDUP'
     * @return string 'PARTICIPANT_ALREADY_SHOWEDUP'
     */
    const VALUE_PARTICIPANT_ALREADY_SHOWEDUP = 'PARTICIPANT_ALREADY_SHOWEDUP';
    /**
     * Constant for value 'ILLEGAL_BOOKING_STATE_FOR_BOOKING'
     * @return string 'ILLEGAL_BOOKING_STATE_FOR_BOOKING'
     */
    const VALUE_ILLEGAL_BOOKING_STATE_FOR_BOOKING = 'ILLEGAL_BOOKING_STATE_FOR_BOOKING';
    /**
     * Constant for value 'LISTS_FULL'
     * @return string 'LISTS_FULL'
     */
    const VALUE_LISTS_FULL = 'LISTS_FULL';
    /**
     * Constant for value 'TOO_LATE_TO_BOOK_PARTICIPANT'
     * @return string 'TOO_LATE_TO_BOOK_PARTICIPANT'
     */
    const VALUE_TOO_LATE_TO_BOOK_PARTICIPANT = 'TOO_LATE_TO_BOOK_PARTICIPANT';
    /**
     * Constant for value 'PARTICIPANT_OCCUPIED'
     * @return string 'PARTICIPANT_OCCUPIED'
     */
    const VALUE_PARTICIPANT_OCCUPIED = 'PARTICIPANT_OCCUPIED';
    /**
     * Constant for value 'FULL_BOOKING_RESTRICTION'
     * @return string 'FULL_BOOKING_RESTRICTION'
     */
    const VALUE_FULL_BOOKING_RESTRICTION = 'FULL_BOOKING_RESTRICTION';
    /**
     * Constant for value 'IN_ADVANCE_LIMIT_BOOKING_RESTRICTION'
     * @return string 'IN_ADVANCE_LIMIT_BOOKING_RESTRICTION'
     */
    const VALUE_IN_ADVANCE_LIMIT_BOOKING_RESTRICTION = 'IN_ADVANCE_LIMIT_BOOKING_RESTRICTION';
    /**
     * Constant for value 'PERSON_DOUBLE_PARTICIPATION'
     * @return string 'PERSON_DOUBLE_PARTICIPATION'
     */
    const VALUE_PERSON_DOUBLE_PARTICIPATION = 'PERSON_DOUBLE_PARTICIPATION';
    /**
     * Constant for value 'PARTICIPANT_BLACKLISTED'
     * @return string 'PARTICIPANT_BLACKLISTED'
     */
    const VALUE_PARTICIPANT_BLACKLISTED = 'PARTICIPANT_BLACKLISTED';
    /**
     * Constant for value 'TOO_LATE_TO_CANCEL_BOOKING'
     * @return string 'TOO_LATE_TO_CANCEL_BOOKING'
     */
    const VALUE_TOO_LATE_TO_CANCEL_BOOKING = 'TOO_LATE_TO_CANCEL_BOOKING';
    /**
     * Constant for value 'TOO_LATE_TO_CANCEL_BY_CENTER'
     * @return string 'TOO_LATE_TO_CANCEL_BY_CENTER'
     */
    const VALUE_TOO_LATE_TO_CANCEL_BY_CENTER = 'TOO_LATE_TO_CANCEL_BY_CENTER';
    /**
     * Constant for value 'TOO_LATE_TO_CANCEL_BY_CUSTOMER'
     * @return string 'TOO_LATE_TO_CANCEL_BY_CUSTOMER'
     */
    const VALUE_TOO_LATE_TO_CANCEL_BY_CUSTOMER = 'TOO_LATE_TO_CANCEL_BY_CUSTOMER';
    /**
     * Constant for value 'UNABLE_TO_REFRESH_WAITING_LIST'
     * @return string 'UNABLE_TO_REFRESH_WAITING_LIST'
     */
    const VALUE_UNABLE_TO_REFRESH_WAITING_LIST = 'UNABLE_TO_REFRESH_WAITING_LIST';
    /**
     * Constant for value 'TOO_FEW_PARTICIPANTS'
     * @return string 'TOO_FEW_PARTICIPANTS'
     */
    const VALUE_TOO_FEW_PARTICIPANTS = 'TOO_FEW_PARTICIPANTS';
    /**
     * Constant for value 'FREEZE_MAX_PREBOOK_PERIOD'
     * @return string 'FREEZE_MAX_PREBOOK_PERIOD'
     */
    const VALUE_FREEZE_MAX_PREBOOK_PERIOD = 'FREEZE_MAX_PREBOOK_PERIOD';
    /**
     * Constant for value 'FREEZE_MIN_OR_MAX_DURATION'
     * @return string 'FREEZE_MIN_OR_MAX_DURATION'
     */
    const VALUE_FREEZE_MIN_OR_MAX_DURATION = 'FREEZE_MIN_OR_MAX_DURATION';
    /**
     * Constant for value 'FREEZE_TOO_LONG'
     * @return string 'FREEZE_TOO_LONG'
     */
    const VALUE_FREEZE_TOO_LONG = 'FREEZE_TOO_LONG';
    /**
     * Constant for value 'FREEZE_TOO_SHORT'
     * @return string 'FREEZE_TOO_SHORT'
     */
    const VALUE_FREEZE_TOO_SHORT = 'FREEZE_TOO_SHORT';
    /**
     * Constant for value 'FREEZE_MAX_NUMBER_REACHED'
     * @return string 'FREEZE_MAX_NUMBER_REACHED'
     */
    const VALUE_FREEZE_MAX_NUMBER_REACHED = 'FREEZE_MAX_NUMBER_REACHED';
    /**
     * Constant for value 'TOO_MANY_FREEZES_PERIOD'
     * @return string 'TOO_MANY_FREEZES_PERIOD'
     */
    const VALUE_TOO_MANY_FREEZES_PERIOD = 'TOO_MANY_FREEZES_PERIOD';
    /**
     * Constant for value 'TOO_MANY_FREEZES_TOTAL'
     * @return string 'TOO_MANY_FREEZES_TOTAL'
     */
    const VALUE_TOO_MANY_FREEZES_TOTAL = 'TOO_MANY_FREEZES_TOTAL';
    /**
     * Constant for value 'FREEZE_MAX_TOTAL_DURATION_REACHED'
     * @return string 'FREEZE_MAX_TOTAL_DURATION_REACHED'
     */
    const VALUE_FREEZE_MAX_TOTAL_DURATION_REACHED = 'FREEZE_MAX_TOTAL_DURATION_REACHED';
    /**
     * Constant for value 'FREEZE_NOT_ALLOWED'
     * @return string 'FREEZE_NOT_ALLOWED'
     */
    const VALUE_FREEZE_NOT_ALLOWED = 'FREEZE_NOT_ALLOWED';
    /**
     * Constant for value 'FREEZE_START_AND_STOP_DATE'
     * @return string 'FREEZE_START_AND_STOP_DATE'
     */
    const VALUE_FREEZE_START_AND_STOP_DATE = 'FREEZE_START_AND_STOP_DATE';
    /**
     * Constant for value 'FREEZE_START_IN_THE_PAST'
     * @return string 'FREEZE_START_IN_THE_PAST'
     */
    const VALUE_FREEZE_START_IN_THE_PAST = 'FREEZE_START_IN_THE_PAST';
    /**
     * Constant for value 'FREEZE_END_BEFORE_SUBSCRIPTION_END'
     * @return string 'FREEZE_END_BEFORE_SUBSCRIPTION_END'
     */
    const VALUE_FREEZE_END_BEFORE_SUBSCRIPTION_END = 'FREEZE_END_BEFORE_SUBSCRIPTION_END';
    /**
     * Constant for value 'FREEZE_START_AFTER_SUBSCRIPTION_END'
     * @return string 'FREEZE_START_AFTER_SUBSCRIPTION_END'
     */
    const VALUE_FREEZE_START_AFTER_SUBSCRIPTION_END = 'FREEZE_START_AFTER_SUBSCRIPTION_END';
    /**
     * Constant for value 'FREEZE_START_BEFORE_SUBSCRIPTION_START'
     * @return string 'FREEZE_START_BEFORE_SUBSCRIPTION_START'
     */
    const VALUE_FREEZE_START_BEFORE_SUBSCRIPTION_START = 'FREEZE_START_BEFORE_SUBSCRIPTION_START';
    /**
     * Constant for value 'FREEZE_OVERLAP'
     * @return string 'FREEZE_OVERLAP'
     */
    const VALUE_FREEZE_OVERLAP = 'FREEZE_OVERLAP';
    /**
     * Constant for value 'FREEZE_FUTURE_FREEZE'
     * @return string 'FREEZE_FUTURE_FREEZE'
     */
    const VALUE_FREEZE_FUTURE_FREEZE = 'FREEZE_FUTURE_FREEZE';
    /**
     * Constant for value 'BOOKINGS_IN_FREEZE_PERIOD'
     * @return string 'BOOKINGS_IN_FREEZE_PERIOD'
     */
    const VALUE_BOOKINGS_IN_FREEZE_PERIOD = 'BOOKINGS_IN_FREEZE_PERIOD';
    /**
     * Constant for value 'NEGATIVE_SUBSCRIPTION_PRICE'
     * @return string 'NEGATIVE_SUBSCRIPTION_PRICE'
     */
    const VALUE_NEGATIVE_SUBSCRIPTION_PRICE = 'NEGATIVE_SUBSCRIPTION_PRICE';
    /**
     * Constant for value 'SUBSCRIPTION_INCORRECT_STATE'
     * @return string 'SUBSCRIPTION_INCORRECT_STATE'
     */
    const VALUE_SUBSCRIPTION_INCORRECT_STATE = 'SUBSCRIPTION_INCORRECT_STATE';
    /**
     * Constant for value 'SUBSCRIPTION_INCORRECT_TYPE'
     * @return string 'SUBSCRIPTION_INCORRECT_TYPE'
     */
    const VALUE_SUBSCRIPTION_INCORRECT_TYPE = 'SUBSCRIPTION_INCORRECT_TYPE';
    /**
     * Constant for value 'START_DATE_BEFORE_BILLED_UNTIL_DATE'
     * @return string 'START_DATE_BEFORE_BILLED_UNTIL_DATE'
     */
    const VALUE_START_DATE_BEFORE_BILLED_UNTIL_DATE = 'START_DATE_BEFORE_BILLED_UNTIL_DATE';
    /**
     * Constant for value 'PARTICIPATION_ALREADY_CANCELLED'
     * @return string 'PARTICIPATION_ALREADY_CANCELLED'
     */
    const VALUE_PARTICIPATION_ALREADY_CANCELLED = 'PARTICIPATION_ALREADY_CANCELLED';
    /**
     * Constant for value 'PARTICIPATION_CANCELLED'
     * @return string 'PARTICIPATION_CANCELLED'
     */
    const VALUE_PARTICIPATION_CANCELLED = 'PARTICIPATION_CANCELLED';
    /**
     * Constant for value 'MINIMUM_AGE_EXCEPTION'
     * @return string 'MINIMUM_AGE_EXCEPTION'
     */
    const VALUE_MINIMUM_AGE_EXCEPTION = 'MINIMUM_AGE_EXCEPTION';
    /**
     * Constant for value 'TEMPLATE_NOT_DEFINED'
     * @return string 'TEMPLATE_NOT_DEFINED'
     */
    const VALUE_TEMPLATE_NOT_DEFINED = 'TEMPLATE_NOT_DEFINED';
    /**
     * Constant for value 'INVALID_OTHER_PAYER_TYPE'
     * @return string 'INVALID_OTHER_PAYER_TYPE'
     */
    const VALUE_INVALID_OTHER_PAYER_TYPE = 'INVALID_OTHER_PAYER_TYPE';
    /**
     * Constant for value 'DIFFERENT_COUNTRY_FOR_OTHER_PAYER'
     * @return string 'DIFFERENT_COUNTRY_FOR_OTHER_PAYER'
     */
    const VALUE_DIFFERENT_COUNTRY_FOR_OTHER_PAYER = 'DIFFERENT_COUNTRY_FOR_OTHER_PAYER';
    /**
     * Constant for value 'PERSON_IS_NOT_STAFF'
     * @return string 'PERSON_IS_NOT_STAFF'
     */
    const VALUE_PERSON_IS_NOT_STAFF = 'PERSON_IS_NOT_STAFF';
    /**
     * Constant for value 'OTHER_PAYER_HAS_OTHER_PAYER'
     * @return string 'OTHER_PAYER_HAS_OTHER_PAYER'
     */
    const VALUE_OTHER_PAYER_HAS_OTHER_PAYER = 'OTHER_PAYER_HAS_OTHER_PAYER';
    /**
     * Constant for value 'NO_VALID_ACCOUNT_FOR_OTHER_PAYER'
     * @return string 'NO_VALID_ACCOUNT_FOR_OTHER_PAYER'
     */
    const VALUE_NO_VALID_ACCOUNT_FOR_OTHER_PAYER = 'NO_VALID_ACCOUNT_FOR_OTHER_PAYER';
    /**
     * Constant for value 'PERSON_STATUS_NOT_VALID_FOR_ATTEND'
     * @return string 'PERSON_STATUS_NOT_VALID_FOR_ATTEND'
     */
    const VALUE_PERSON_STATUS_NOT_VALID_FOR_ATTEND = 'PERSON_STATUS_NOT_VALID_FOR_ATTEND';
    /**
     * Constant for value 'ATTENDING_PERSON_BLACKLISTED'
     * @return string 'ATTENDING_PERSON_BLACKLISTED'
     */
    const VALUE_ATTENDING_PERSON_BLACKLISTED = 'ATTENDING_PERSON_BLACKLISTED';
    /**
     * Constant for value 'MISSING_PRIVILEGE_FOR_ATTEND'
     * @return string 'MISSING_PRIVILEGE_FOR_ATTEND'
     */
    const VALUE_MISSING_PRIVILEGE_FOR_ATTEND = 'MISSING_PRIVILEGE_FOR_ATTEND';
    /**
     * Constant for value 'ATTEND_NOT_POSSIBLE_AT_SPECIFIED_TIME'
     * @return string 'ATTEND_NOT_POSSIBLE_AT_SPECIFIED_TIME'
     */
    const VALUE_ATTEND_NOT_POSSIBLE_AT_SPECIFIED_TIME = 'ATTEND_NOT_POSSIBLE_AT_SPECIFIED_TIME';
    /**
     * Constant for value 'CLIPCARD_NOT_USABLE'
     * @return string 'CLIPCARD_NOT_USABLE'
     */
    const VALUE_CLIPCARD_NOT_USABLE = 'CLIPCARD_NOT_USABLE';
    /**
     * Constant for value 'TOO_MANY_CLIPS_ON_CLIPCARD'
     * @return string 'TOO_MANY_CLIPS_ON_CLIPCARD'
     */
    const VALUE_TOO_MANY_CLIPS_ON_CLIPCARD = 'TOO_MANY_CLIPS_ON_CLIPCARD';
    /**
     * Constant for value 'NOT_ENOUGH_CLIPS_ON_CLIPCARD'
     * @return string 'NOT_ENOUGH_CLIPS_ON_CLIPCARD'
     */
    const VALUE_NOT_ENOUGH_CLIPS_ON_CLIPCARD = 'NOT_ENOUGH_CLIPS_ON_CLIPCARD';
    /**
     * Constant for value 'LOGIN_BLOCKED'
     * @return string 'LOGIN_BLOCKED'
     */
    const VALUE_LOGIN_BLOCKED = 'LOGIN_BLOCKED';
    /**
     * Constant for value 'PASSWORD_EXPIRED'
     * @return string 'PASSWORD_EXPIRED'
     */
    const VALUE_PASSWORD_EXPIRED = 'PASSWORD_EXPIRED';
    /**
     * Constant for value 'PERSON_DELETED'
     * @return string 'PERSON_DELETED'
     */
    const VALUE_PERSON_DELETED = 'PERSON_DELETED';
    /**
     * Constant for value 'PERSON_ANONYMIZED'
     * @return string 'PERSON_ANONYMIZED'
     */
    const VALUE_PERSON_ANONYMIZED = 'PERSON_ANONYMIZED';
    /**
     * Constant for value 'INVALID_PICTURE'
     * @return string 'INVALID_PICTURE'
     */
    const VALUE_INVALID_PICTURE = 'INVALID_PICTURE';
    /**
     * Constant for value 'PERSON_NOT_FOUND_FOR_VENDING'
     * @return string 'PERSON_NOT_FOUND_FOR_VENDING'
     */
    const VALUE_PERSON_NOT_FOUND_FOR_VENDING = 'PERSON_NOT_FOUND_FOR_VENDING';
    /**
     * Constant for value 'TOO_MANY_MATCHES'
     * @return string 'TOO_MANY_MATCHES'
     */
    const VALUE_TOO_MANY_MATCHES = 'TOO_MANY_MATCHES';
    /**
     * Constant for value 'PASSWORD_NOT_SET'
     * @return string 'PASSWORD_NOT_SET'
     */
    const VALUE_PASSWORD_NOT_SET = 'PASSWORD_NOT_SET';
    /**
     * Constant for value 'PASSWORD_TOO_SHORT'
     * @return string 'PASSWORD_TOO_SHORT'
     */
    const VALUE_PASSWORD_TOO_SHORT = 'PASSWORD_TOO_SHORT';
    /**
     * Constant for value 'CANNOT_PARTICIPATE_IN_FREEZE_PERIOD'
     * @return string 'CANNOT_PARTICIPATE_IN_FREEZE_PERIOD'
     */
    const VALUE_CANNOT_PARTICIPATE_IN_FREEZE_PERIOD = 'CANNOT_PARTICIPATE_IN_FREEZE_PERIOD';
    /**
     * Constant for value 'INVALID_STATUS'
     * @return string 'INVALID_STATUS'
     */
    const VALUE_INVALID_STATUS = 'INVALID_STATUS';
    /**
     * Constant for value 'CONCURRENT_CALLS'
     * @return string 'CONCURRENT_CALLS'
     */
    const VALUE_CONCURRENT_CALLS = 'CONCURRENT_CALLS';
    /**
     * Constant for value 'CANNOT_UPDATE_COMPLETED_QUESTIONNAIRE'
     * @return string 'CANNOT_UPDATE_COMPLETED_QUESTIONNAIRE'
     */
    const VALUE_CANNOT_UPDATE_COMPLETED_QUESTIONNAIRE = 'CANNOT_UPDATE_COMPLETED_QUESTIONNAIRE';
    /**
     * Constant for value 'MISSING_ADMIN_FEE_PRODUCT'
     * @return string 'MISSING_ADMIN_FEE_PRODUCT'
     */
    const VALUE_MISSING_ADMIN_FEE_PRODUCT = 'MISSING_ADMIN_FEE_PRODUCT';
    /**
     * Constant for value 'LOGIN_NOT_VALID'
     * @return string 'LOGIN_NOT_VALID'
     */
    const VALUE_LOGIN_NOT_VALID = 'LOGIN_NOT_VALID';
    /**
     * Constant for value 'ACTIVE_TASKS_ALREADY_EXISTS_FOR_PERSON'
     * @return string 'ACTIVE_TASKS_ALREADY_EXISTS_FOR_PERSON'
     */
    const VALUE_ACTIVE_TASKS_ALREADY_EXISTS_FOR_PERSON = 'ACTIVE_TASKS_ALREADY_EXISTS_FOR_PERSON';
    /**
     * Constant for value 'TASK_TYPE_EXTERNAL_KEY_DOES_NOT_EXIST'
     * @return string 'TASK_TYPE_EXTERNAL_KEY_DOES_NOT_EXIST'
     */
    const VALUE_TASK_TYPE_EXTERNAL_KEY_DOES_NOT_EXIST = 'TASK_TYPE_EXTERNAL_KEY_DOES_NOT_EXIST';
    /**
     * Constant for value 'SUBSCRIPTION_OF_SAME_TYPE'
     * @return string 'SUBSCRIPTION_OF_SAME_TYPE'
     */
    const VALUE_SUBSCRIPTION_OF_SAME_TYPE = 'SUBSCRIPTION_OF_SAME_TYPE';
    /**
     * Constant for value 'PERSON_NOT_FOUND'
     * @return string 'PERSON_NOT_FOUND'
     */
    const VALUE_PERSON_NOT_FOUND = 'PERSON_NOT_FOUND';
    /**
     * Constant for value 'EMPLOYEE_KEY_NOT_FOUND'
     * @return string 'EMPLOYEE_KEY_NOT_FOUND'
     */
    const VALUE_EMPLOYEE_KEY_NOT_FOUND = 'EMPLOYEE_KEY_NOT_FOUND';
    /**
     * Constant for value 'EMPLOYEE_LOGIN_ALREADY_BLOCKED'
     * @return string 'EMPLOYEE_LOGIN_ALREADY_BLOCKED'
     */
    const VALUE_EMPLOYEE_LOGIN_ALREADY_BLOCKED = 'EMPLOYEE_LOGIN_ALREADY_BLOCKED';
    /**
     * Constant for value 'EMPLOYEE_LOGIN_ALREADY_UNBLOCKED'
     * @return string 'EMPLOYEE_LOGIN_ALREADY_UNBLOCKED'
     */
    const VALUE_EMPLOYEE_LOGIN_ALREADY_UNBLOCKED = 'EMPLOYEE_LOGIN_ALREADY_UNBLOCKED';
    /**
     * Constant for value 'INVALID_EMAIL'
     * @return string 'INVALID_EMAIL'
     */
    const VALUE_INVALID_EMAIL = 'INVALID_EMAIL';
    /**
     * Constant for value 'CENTER_NOT_FOUND'
     * @return string 'CENTER_NOT_FOUND'
     */
    const VALUE_CENTER_NOT_FOUND = 'CENTER_NOT_FOUND';
    /**
     * Constant for value 'CENTER_ATTRIBUTE_TYPE_NOT_STRING'
     * @return string 'CENTER_ATTRIBUTE_TYPE_NOT_STRING'
     */
    const VALUE_CENTER_ATTRIBUTE_TYPE_NOT_STRING = 'CENTER_ATTRIBUTE_TYPE_NOT_STRING';
    /**
     * Constant for value 'CENTER_ATTRIBUTE_NOT_FOUND'
     * @return string 'CENTER_ATTRIBUTE_NOT_FOUND'
     */
    const VALUE_CENTER_ATTRIBUTE_NOT_FOUND = 'CENTER_ATTRIBUTE_NOT_FOUND';
    /**
     * Constant for value 'CENTER_ATTRIBUTE_UPDATE_NOT_ALLOWED'
     * @return string 'CENTER_ATTRIBUTE_UPDATE_NOT_ALLOWED'
     */
    const VALUE_CENTER_ATTRIBUTE_UPDATE_NOT_ALLOWED = 'CENTER_ATTRIBUTE_UPDATE_NOT_ALLOWED';
    /**
     * Constant for value 'CENTER_ATTRIBUTE_VALUE_NOT_ALLOWED'
     * @return string 'CENTER_ATTRIBUTE_VALUE_NOT_ALLOWED'
     */
    const VALUE_CENTER_ATTRIBUTE_VALUE_NOT_ALLOWED = 'CENTER_ATTRIBUTE_VALUE_NOT_ALLOWED';
    /**
     * Constant for value 'DEDUCTION_DAY_VALIDATION_FAILED'
     * @return string 'DEDUCTION_DAY_VALIDATION_FAILED'
     */
    const VALUE_DEDUCTION_DAY_VALIDATION_FAILED = 'DEDUCTION_DAY_VALIDATION_FAILED';
    /**
     * Constant for value 'CHILD_MUST_HAVE_AT_LEAST_ONE_PARENT'
     * @return string 'CHILD_MUST_HAVE_AT_LEAST_ONE_PARENT'
     */
    const VALUE_CHILD_MUST_HAVE_AT_LEAST_ONE_PARENT = 'CHILD_MUST_HAVE_AT_LEAST_ONE_PARENT';
    /**
     * Constant for value 'CHILD_HAS_TOO_MANY_PARENTS'
     * @return string 'CHILD_HAS_TOO_MANY_PARENTS'
     */
    const VALUE_CHILD_HAS_TOO_MANY_PARENTS = 'CHILD_HAS_TOO_MANY_PARENTS';
    /**
     * Constant for value 'PARENT_HAS_TOO_MANY_CHILDREN'
     * @return string 'PARENT_HAS_TOO_MANY_CHILDREN'
     */
    const VALUE_PARENT_HAS_TOO_MANY_CHILDREN = 'PARENT_HAS_TOO_MANY_CHILDREN';
    /**
     * Constant for value 'CHILD_PARENT_RELATIONSHIP_ALREADY_EXISTS'
     * @return string 'CHILD_PARENT_RELATIONSHIP_ALREADY_EXISTS'
     */
    const VALUE_CHILD_PARENT_RELATIONSHIP_ALREADY_EXISTS = 'CHILD_PARENT_RELATIONSHIP_ALREADY_EXISTS';
    /**
     * Constant for value 'CHILD_PARENT_RELATIONSHIP_DOES_NOT_EXIST'
     * @return string 'CHILD_PARENT_RELATIONSHIP_DOES_NOT_EXIST'
     */
    const VALUE_CHILD_PARENT_RELATIONSHIP_DOES_NOT_EXIST = 'CHILD_PARENT_RELATIONSHIP_DOES_NOT_EXIST';
    /**
     * Constant for value 'GUARDIAN_IS_TOO_YOUNG'
     * @return string 'GUARDIAN_IS_TOO_YOUNG'
     */
    const VALUE_GUARDIAN_IS_TOO_YOUNG = 'GUARDIAN_IS_TOO_YOUNG';
    /**
     * Constant for value 'CHILD_IS_TOO_OLD'
     * @return string 'CHILD_IS_TOO_OLD'
     */
    const VALUE_CHILD_IS_TOO_OLD = 'CHILD_IS_TOO_OLD';
    /**
     * Constant for value 'AGE_NOT_SET'
     * @return string 'AGE_NOT_SET'
     */
    const VALUE_AGE_NOT_SET = 'AGE_NOT_SET';
    /**
     * Constant for value 'PAYMENT_AGREEMENT_PERSON_MISMATCH'
     * @return string 'PAYMENT_AGREEMENT_PERSON_MISMATCH'
     */
    const VALUE_PAYMENT_AGREEMENT_PERSON_MISMATCH = 'PAYMENT_AGREEMENT_PERSON_MISMATCH';
    /**
     * Constant for value 'PAYMENT_CYCLE_TYPE_NOT_ALLOWED_EXCEPTION'
     * @return string 'PAYMENT_CYCLE_TYPE_NOT_ALLOWED_EXCEPTION'
     */
    const VALUE_PAYMENT_CYCLE_TYPE_NOT_ALLOWED_EXCEPTION = 'PAYMENT_CYCLE_TYPE_NOT_ALLOWED_EXCEPTION';
    /**
     * Constant for value 'SUBSCRIPTION_DOES_NOT_BELONG_TO_MEMBER'
     * @return string 'SUBSCRIPTION_DOES_NOT_BELONG_TO_MEMBER'
     */
    const VALUE_SUBSCRIPTION_DOES_NOT_BELONG_TO_MEMBER = 'SUBSCRIPTION_DOES_NOT_BELONG_TO_MEMBER';
    /**
     * Constant for value 'CANNOT_DELETE_SUBSCRIPTION'
     * @return string 'CANNOT_DELETE_SUBSCRIPTION'
     */
    const VALUE_CANNOT_DELETE_SUBSCRIPTION = 'CANNOT_DELETE_SUBSCRIPTION';
    /**
     * Constant for value 'NO_PAYMENT_AGREEMENT_FOUND'
     * @return string 'NO_PAYMENT_AGREEMENT_FOUND'
     */
    const VALUE_NO_PAYMENT_AGREEMENT_FOUND = 'NO_PAYMENT_AGREEMENT_FOUND';
    /**
     * Constant for value 'PAYMENT_AGREEMENT_NOT_VALID_FOR_SALES'
     * @return string 'PAYMENT_AGREEMENT_NOT_VALID_FOR_SALES'
     */
    const VALUE_PAYMENT_AGREEMENT_NOT_VALID_FOR_SALES = 'PAYMENT_AGREEMENT_NOT_VALID_FOR_SALES';
    /**
     * Constant for value 'PAYMENT_AGREEMENT_NOT_ACTIVE'
     * @return string 'PAYMENT_AGREEMENT_NOT_ACTIVE'
     */
    const VALUE_PAYMENT_AGREEMENT_NOT_ACTIVE = 'PAYMENT_AGREEMENT_NOT_ACTIVE';
    /**
     * Constant for value 'PAYMENT_CYCLE_NOT_ALLOWED'
     * @return string 'PAYMENT_CYCLE_NOT_ALLOWED'
     */
    const VALUE_PAYMENT_CYCLE_NOT_ALLOWED = 'PAYMENT_CYCLE_NOT_ALLOWED';
    /**
     * Constant for value 'SUBSCRIPTION_SALE_OUTSIDE_HOME_CLUB_NOT_ALLOWED'
     * @return string 'SUBSCRIPTION_SALE_OUTSIDE_HOME_CLUB_NOT_ALLOWED'
     */
    const VALUE_SUBSCRIPTION_SALE_OUTSIDE_HOME_CLUB_NOT_ALLOWED = 'SUBSCRIPTION_SALE_OUTSIDE_HOME_CLUB_NOT_ALLOWED';
    /**
     * Constant for value 'BELOW_REQUIRED_AGE_FOR_EMAIL_CAPTURE'
     * @return string 'BELOW_REQUIRED_AGE_FOR_EMAIL_CAPTURE'
     */
    const VALUE_BELOW_REQUIRED_AGE_FOR_EMAIL_CAPTURE = 'BELOW_REQUIRED_AGE_FOR_EMAIL_CAPTURE';
    /**
     * Constant for value 'ROLE_NOT_FOUND'
     * @return string 'ROLE_NOT_FOUND'
     */
    const VALUE_ROLE_NOT_FOUND = 'ROLE_NOT_FOUND';
    /**
     * Constant for value 'ACTION_NOT_ALLOWED'
     * @return string 'ACTION_NOT_ALLOWED'
     */
    const VALUE_ACTION_NOT_ALLOWED = 'ACTION_NOT_ALLOWED';
    /**
     * Constant for value 'SCOPE_NOT_FOUND'
     * @return string 'SCOPE_NOT_FOUND'
     */
    const VALUE_SCOPE_NOT_FOUND = 'SCOPE_NOT_FOUND';
    /**
     * Constant for value 'EMPLOYEE_BLOCKED'
     * @return string 'EMPLOYEE_BLOCKED'
     */
    const VALUE_EMPLOYEE_BLOCKED = 'EMPLOYEE_BLOCKED';
    /**
     * Constant for value 'ROLE_NOT_ASSIGNED'
     * @return string 'ROLE_NOT_ASSIGNED'
     */
    const VALUE_ROLE_NOT_ASSIGNED = 'ROLE_NOT_ASSIGNED';
    /**
     * Constant for value 'INVALID_EMPLOYEE_KEY'
     * @return string 'INVALID_EMPLOYEE_KEY'
     */
    const VALUE_INVALID_EMPLOYEE_KEY = 'INVALID_EMPLOYEE_KEY';
    /**
     * Constant for value 'MISSING_PRIVILEGES'
     * @return string 'MISSING_PRIVILEGES'
     */
    const VALUE_MISSING_PRIVILEGES = 'MISSING_PRIVILEGES';
    /**
     * Constant for value 'DATE_PARSE_ERROR'
     * @return string 'DATE_PARSE_ERROR'
     */
    const VALUE_DATE_PARSE_ERROR = 'DATE_PARSE_ERROR';
    /**
     * Constant for value 'DATE_INVALID'
     * @return string 'DATE_INVALID'
     */
    const VALUE_DATE_INVALID = 'DATE_INVALID';
    /**
     * Constant for value 'CLIPCARD_NOT_ACTIVE'
     * @return string 'CLIPCARD_NOT_ACTIVE'
     */
    const VALUE_CLIPCARD_NOT_ACTIVE = 'CLIPCARD_NOT_ACTIVE';
    /**
     * Constant for value 'GROUP_ALREADY_EXIST'
     * @return string 'GROUP_ALREADY_EXIST'
     */
    const VALUE_GROUP_ALREADY_EXIST = 'GROUP_ALREADY_EXIST';
    /**
     * Constant for value 'EMPLOYEE_AND_PERSON_KEY_SET'
     * @return string 'EMPLOYEE_AND_PERSON_KEY_SET'
     */
    const VALUE_EMPLOYEE_AND_PERSON_KEY_SET = 'EMPLOYEE_AND_PERSON_KEY_SET';
    /**
     * Constant for value 'MULTIPLE_EMPLOYEE_LOGINS_FOUND'
     * @return string 'MULTIPLE_EMPLOYEE_LOGINS_FOUND'
     */
    const VALUE_MULTIPLE_EMPLOYEE_LOGINS_FOUND = 'MULTIPLE_EMPLOYEE_LOGINS_FOUND';
    /**
     * Constant for value 'MULTIPLE_BLOCKED_EMPLOYEE_LOGINS_FOUND'
     * @return string 'MULTIPLE_BLOCKED_EMPLOYEE_LOGINS_FOUND'
     */
    const VALUE_MULTIPLE_BLOCKED_EMPLOYEE_LOGINS_FOUND = 'MULTIPLE_BLOCKED_EMPLOYEE_LOGINS_FOUND';
    /**
     * Constant for value 'NO_ACTIVE_EMPLOYEE_LOGINS_FOUND'
     * @return string 'NO_ACTIVE_EMPLOYEE_LOGINS_FOUND'
     */
    const VALUE_NO_ACTIVE_EMPLOYEE_LOGINS_FOUND = 'NO_ACTIVE_EMPLOYEE_LOGINS_FOUND';
    /**
     * Constant for value 'NO_BLOCKED_EMPLOYEE_LOGINS_FOUND'
     * @return string 'NO_BLOCKED_EMPLOYEE_LOGINS_FOUND'
     */
    const VALUE_NO_BLOCKED_EMPLOYEE_LOGINS_FOUND = 'NO_BLOCKED_EMPLOYEE_LOGINS_FOUND';
    /**
     * Constant for value 'INVOICE_LINE_NOT_FOUND'
     * @return string 'INVOICE_LINE_NOT_FOUND'
     */
    const VALUE_INVOICE_LINE_NOT_FOUND = 'INVOICE_LINE_NOT_FOUND';
    /**
     * Constant for value 'INVALID_CLIP_SIZE'
     * @return string 'INVALID_CLIP_SIZE'
     */
    const VALUE_INVALID_CLIP_SIZE = 'INVALID_CLIP_SIZE';
    /**
     * Constant for value 'CLIP_SIZE_NOT_SUPPORTED'
     * @return string 'CLIP_SIZE_NOT_SUPPORTED'
     */
    const VALUE_CLIP_SIZE_NOT_SUPPORTED = 'CLIP_SIZE_NOT_SUPPORTED';
    /**
     * Constant for value 'MISSING_CLIP_SIZE'
     * @return string 'MISSING_CLIP_SIZE'
     */
    const VALUE_MISSING_CLIP_SIZE = 'MISSING_CLIP_SIZE';
    /**
     * Constant for value 'DUPLICATE_ENTITY_FOUND'
     * @return string 'DUPLICATE_ENTITY_FOUND'
     */
    const VALUE_DUPLICATE_ENTITY_FOUND = 'DUPLICATE_ENTITY_FOUND';
    /**
     * Constant for value 'RELATION_ALREADY_EXISTS'
     * @return string 'RELATION_ALREADY_EXISTS'
     */
    const VALUE_RELATION_ALREADY_EXISTS = 'RELATION_ALREADY_EXISTS';
    /**
     * Constant for value 'RELATION_DOESNT_EXIST'
     * @return string 'RELATION_DOESNT_EXIST'
     */
    const VALUE_RELATION_DOESNT_EXIST = 'RELATION_DOESNT_EXIST';
    /**
     * Constant for value 'FEATURE_NOT_ENABLED'
     * @return string 'FEATURE_NOT_ENABLED'
     */
    const VALUE_FEATURE_NOT_ENABLED = 'FEATURE_NOT_ENABLED';
    /**
     * Constant for value 'BUYOUT_OVERRIDE_TOO_HIGH'
     * @return string 'BUYOUT_OVERRIDE_TOO_HIGH'
     */
    const VALUE_BUYOUT_OVERRIDE_TOO_HIGH = 'BUYOUT_OVERRIDE_TOO_HIGH';
    /**
     * Constant for value 'CLIPCARD_NOT_FOUND'
     * @return string 'CLIPCARD_NOT_FOUND'
     */
    const VALUE_CLIPCARD_NOT_FOUND = 'CLIPCARD_NOT_FOUND';
    /**
     * Constant for value 'SUBSCRIPTION_WRONG_TYPE'
     * @return string 'SUBSCRIPTION_WRONG_TYPE'
     */
    const VALUE_SUBSCRIPTION_WRONG_TYPE = 'SUBSCRIPTION_WRONG_TYPE';
    /**
     * Constant for value 'INVALID_PAYMENT_CYCLE'
     * @return string 'INVALID_PAYMENT_CYCLE'
     */
    const VALUE_INVALID_PAYMENT_CYCLE = 'INVALID_PAYMENT_CYCLE';
    /**
     * Constant for value 'INVALID_COURSE_BOOKING'
     * @return string 'INVALID_COURSE_BOOKING'
     */
    const VALUE_INVALID_COURSE_BOOKING = 'INVALID_COURSE_BOOKING';
    /**
     * Constant for value 'INVALID_COURSE_PRIVILEGE'
     * @return string 'INVALID_COURSE_PRIVILEGE'
     */
    const VALUE_INVALID_COURSE_PRIVILEGE = 'INVALID_COURSE_PRIVILEGE';
    /**
     * Constant for value 'CLIPCARD_EXPIRED'
     * @return string 'CLIPCARD_EXPIRED'
     */
    const VALUE_CLIPCARD_EXPIRED = 'CLIPCARD_EXPIRED';
    /**
     * Constant for value 'CLIPCARD_OVERDUE'
     * @return string 'CLIPCARD_OVERDUE'
     */
    const VALUE_CLIPCARD_OVERDUE = 'CLIPCARD_OVERDUE';
    /**
     * Constant for value 'CLIPCARD_REFUNDED'
     * @return string 'CLIPCARD_REFUNDED'
     */
    const VALUE_CLIPCARD_REFUNDED = 'CLIPCARD_REFUNDED';
    /**
     * Constant for value 'NOT_WITHIN_AGE_GROUP'
     * @return string 'NOT_WITHIN_AGE_GROUP'
     */
    const VALUE_NOT_WITHIN_AGE_GROUP = 'NOT_WITHIN_AGE_GROUP';
    /**
     * Constant for value 'TOO_LATE_TO_SIGNUP_TO_COURSE'
     * @return string 'TOO_LATE_TO_SIGNUP_TO_COURSE'
     */
    const VALUE_TOO_LATE_TO_SIGNUP_TO_COURSE = 'TOO_LATE_TO_SIGNUP_TO_COURSE';
    /**
     * Constant for value 'NOT_A_FIXED_COURSE'
     * @return string 'NOT_A_FIXED_COURSE'
     */
    const VALUE_NOT_A_FIXED_COURSE = 'NOT_A_FIXED_COURSE';
    /**
     * Constant for value 'ILLEGAL_PAYMENT_AGREEMENT_STATE'
     * @return string 'ILLEGAL_PAYMENT_AGREEMENT_STATE'
     */
    const VALUE_ILLEGAL_PAYMENT_AGREEMENT_STATE = 'ILLEGAL_PAYMENT_AGREEMENT_STATE';
    /**
     * Constant for value 'MEMBER_CARD_ALREADY_BLOCKED'
     * @return string 'MEMBER_CARD_ALREADY_BLOCKED'
     */
    const VALUE_MEMBER_CARD_ALREADY_BLOCKED = 'MEMBER_CARD_ALREADY_BLOCKED';
    /**
     * Constant for value 'RECURRING_PARTICIPATION'
     * @return string 'RECURRING_PARTICIPATION'
     */
    const VALUE_RECURRING_PARTICIPATION = 'RECURRING_PARTICIPATION';
    /**
     * Constant for value 'ACTIVITY_ACCESS_GROUP_NOT_MATCHING'
     * @return string 'ACTIVITY_ACCESS_GROUP_NOT_MATCHING'
     */
    const VALUE_ACTIVITY_ACCESS_GROUP_NOT_MATCHING = 'ACTIVITY_ACCESS_GROUP_NOT_MATCHING';
    /**
     * Constant for value 'ACTIVITY_RESOURCE_GROUP_NOT_MATCHING'
     * @return string 'ACTIVITY_RESOURCE_GROUP_NOT_MATCHING'
     */
    const VALUE_ACTIVITY_RESOURCE_GROUP_NOT_MATCHING = 'ACTIVITY_RESOURCE_GROUP_NOT_MATCHING';
    /**
     * Constant for value 'ACTIVITY_CAPACITY_TOO_LOW'
     * @return string 'ACTIVITY_CAPACITY_TOO_LOW'
     */
    const VALUE_ACTIVITY_CAPACITY_TOO_LOW = 'ACTIVITY_CAPACITY_TOO_LOW';
    /**
     * Constant for value 'ACTIVITY_CAPACITY_NOT_UNLIMITED'
     * @return string 'ACTIVITY_CAPACITY_NOT_UNLIMITED'
     */
    const VALUE_ACTIVITY_CAPACITY_NOT_UNLIMITED = 'ACTIVITY_CAPACITY_NOT_UNLIMITED';
    /**
     * Constant for value 'NOT_SUPPORTED_FOR_COURSES'
     * @return string 'NOT_SUPPORTED_FOR_COURSES'
     */
    const VALUE_NOT_SUPPORTED_FOR_COURSES = 'NOT_SUPPORTED_FOR_COURSES';
    /**
     * Constant for value 'BOOKING_PROGRAM_NOT_FOUND'
     * @return string 'BOOKING_PROGRAM_NOT_FOUND'
     */
    const VALUE_BOOKING_PROGRAM_NOT_FOUND = 'BOOKING_PROGRAM_NOT_FOUND';
    /**
     * Constant for value 'BOOKING_PROGRAM_STARTED'
     * @return string 'BOOKING_PROGRAM_STARTED'
     */
    const VALUE_BOOKING_PROGRAM_STARTED = 'BOOKING_PROGRAM_STARTED';
    /**
     * Constant for value 'PARTICIPANT_NOT_FOUND_ON_BOOKING_PROGRAM'
     * @return string 'PARTICIPANT_NOT_FOUND_ON_BOOKING_PROGRAM'
     */
    const VALUE_PARTICIPANT_NOT_FOUND_ON_BOOKING_PROGRAM = 'PARTICIPANT_NOT_FOUND_ON_BOOKING_PROGRAM';
    /**
     * Constant for value 'TOO_LATE_TO_REMOVE_BOOKING_PROGRAM_PARTICIPATION'
     * @return string 'TOO_LATE_TO_REMOVE_BOOKING_PROGRAM_PARTICIPATION'
     */
    const VALUE_TOO_LATE_TO_REMOVE_BOOKING_PROGRAM_PARTICIPATION = 'TOO_LATE_TO_REMOVE_BOOKING_PROGRAM_PARTICIPATION';
    /**
     * Constant for value 'TOO_EARLY_TO_SIGNUP_PROGRAM_BOOKING'
     * @return string 'TOO_EARLY_TO_SIGNUP_PROGRAM_BOOKING'
     */
    const VALUE_TOO_EARLY_TO_SIGNUP_PROGRAM_BOOKING = 'TOO_EARLY_TO_SIGNUP_PROGRAM_BOOKING';
    /**
     * Constant for value 'ILLEGAL_DATE_CHANGE'
     * @return string 'ILLEGAL_DATE_CHANGE'
     */
    const VALUE_ILLEGAL_DATE_CHANGE = 'ILLEGAL_DATE_CHANGE';
    /**
     * Constant for value 'MAXIMUM_CHECKED_IN'
     * @return string 'MAXIMUM_CHECKED_IN'
     */
    const VALUE_MAXIMUM_CHECKED_IN = 'MAXIMUM_CHECKED_IN';
    /**
     * Constant for value 'RESOURCE_SELECTION_NOT_ALLOW_BY_ACTIVITY'
     * @return string 'RESOURCE_SELECTION_NOT_ALLOW_BY_ACTIVITY'
     */
    const VALUE_RESOURCE_SELECTION_NOT_ALLOW_BY_ACTIVITY = 'RESOURCE_SELECTION_NOT_ALLOW_BY_ACTIVITY';
    /**
     * Constant for value 'MISSING_RESOURCE'
     * @return string 'MISSING_RESOURCE'
     */
    const VALUE_MISSING_RESOURCE = 'MISSING_RESOURCE';
    /**
     * Constant for value 'WRONG_INSTALLMENT_PLAN'
     * @return string 'WRONG_INSTALLMENT_PLAN'
     */
    const VALUE_WRONG_INSTALLMENT_PLAN = 'WRONG_INSTALLMENT_PLAN';
    /**
     * Constant for value 'INVALID_FREE_PERIOD'
     * @return string 'INVALID_FREE_PERIOD'
     */
    const VALUE_INVALID_FREE_PERIOD = 'INVALID_FREE_PERIOD';
    /**
     * Constant for value 'INVALID_SUBSCRIPTION_STATE'
     * @return string 'INVALID_SUBSCRIPTION_STATE'
     */
    const VALUE_INVALID_SUBSCRIPTION_STATE = 'INVALID_SUBSCRIPTION_STATE';
    /**
     * Constant for value 'INVALID_SUBSCRIPTION_STATE_AND_SUB_STATE'
     * @return string 'INVALID_SUBSCRIPTION_STATE_AND_SUB_STATE'
     */
    const VALUE_INVALID_SUBSCRIPTION_STATE_AND_SUB_STATE = 'INVALID_SUBSCRIPTION_STATE_AND_SUB_STATE';
    /**
     * Constant for value 'SUBSCRIPTION_IS_SPONSORED'
     * @return string 'SUBSCRIPTION_IS_SPONSORED'
     */
    const VALUE_SUBSCRIPTION_IS_SPONSORED = 'SUBSCRIPTION_IS_SPONSORED';
    /**
     * Constant for value 'SUBSCRIPTION_IS_REASSIGNED'
     * @return string 'SUBSCRIPTION_IS_REASSIGNED'
     */
    const VALUE_SUBSCRIPTION_IS_REASSIGNED = 'SUBSCRIPTION_IS_REASSIGNED';
    /**
     * Constant for value 'CAMP_BOOKING_SELECTION_DATE'
     * @return string 'CAMP_BOOKING_SELECTION_DATE'
     */
    const VALUE_CAMP_BOOKING_SELECTION_DATE = 'CAMP_BOOKING_SELECTION_DATE';
    /**
     * Constant for value 'CANNOT_CHANGE_OWNER_IN_EXISTING_PARTICIPATIONS'
     * @return string 'CANNOT_CHANGE_OWNER_IN_EXISTING_PARTICIPATIONS'
     */
    const VALUE_CANNOT_CHANGE_OWNER_IN_EXISTING_PARTICIPATIONS = 'CANNOT_CHANGE_OWNER_IN_EXISTING_PARTICIPATIONS';
    /**
     * Constant for value 'SINGLE_DAYS_BOOKING_NOT_ALLOWED_FOR_CAMP'
     * @return string 'SINGLE_DAYS_BOOKING_NOT_ALLOWED_FOR_CAMP'
     */
    const VALUE_SINGLE_DAYS_BOOKING_NOT_ALLOWED_FOR_CAMP = 'SINGLE_DAYS_BOOKING_NOT_ALLOWED_FOR_CAMP';
    /**
     * Constant for value 'OWNER_BELOW_MINIMUM_AGE'
     * @return string 'OWNER_BELOW_MINIMUM_AGE'
     */
    const VALUE_OWNER_BELOW_MINIMUM_AGE = 'OWNER_BELOW_MINIMUM_AGE';
    /**
     * Constant for value 'OVERRIDE_OF_EXPIRATION_DATE_NOT_ALLOWED'
     * @return string 'OVERRIDE_OF_EXPIRATION_DATE_NOT_ALLOWED'
     */
    const VALUE_OVERRIDE_OF_EXPIRATION_DATE_NOT_ALLOWED = 'OVERRIDE_OF_EXPIRATION_DATE_NOT_ALLOWED';
    /**
     * Constant for value 'MISSING_ISSUE_DATE'
     * @return string 'MISSING_ISSUE_DATE'
     */
    const VALUE_MISSING_ISSUE_DATE = 'MISSING_ISSUE_DATE';
    /**
     * Constant for value 'CUSTOM_JOURNAL_DOCUMENT_TYPE_NOT_ACTIVE'
     * @return string 'CUSTOM_JOURNAL_DOCUMENT_TYPE_NOT_ACTIVE'
     */
    const VALUE_CUSTOM_JOURNAL_DOCUMENT_TYPE_NOT_ACTIVE = 'CUSTOM_JOURNAL_DOCUMENT_TYPE_NOT_ACTIVE';
    /**
     * Constant for value 'CUSTOM_JOURNAL_DOCUMENT_MISSING_ATTACHMENT'
     * @return string 'CUSTOM_JOURNAL_DOCUMENT_MISSING_ATTACHMENT'
     */
    const VALUE_CUSTOM_JOURNAL_DOCUMENT_MISSING_ATTACHMENT = 'CUSTOM_JOURNAL_DOCUMENT_MISSING_ATTACHMENT';
    /**
     * Constant for value 'FAMILY_RELATIVE_RELATIONSHIP_DOES_NOT_EXIST'
     * @return string 'FAMILY_RELATIVE_RELATIONSHIP_DOES_NOT_EXIST'
     */
    const VALUE_FAMILY_RELATIVE_RELATIONSHIP_DOES_NOT_EXIST = 'FAMILY_RELATIVE_RELATIONSHIP_DOES_NOT_EXIST';
    /**
     * Constant for value 'CAMP_ELECTIVE_SELECTED_WITHOUT_CAMP_BOOKING'
     * @return string 'CAMP_ELECTIVE_SELECTED_WITHOUT_CAMP_BOOKING'
     */
    const VALUE_CAMP_ELECTIVE_SELECTED_WITHOUT_CAMP_BOOKING = 'CAMP_ELECTIVE_SELECTED_WITHOUT_CAMP_BOOKING';
    /**
     * Constant for value 'PROGRAM_TYPE_NOT_AVAILABLE'
     * @return string 'PROGRAM_TYPE_NOT_AVAILABLE'
     */
    const VALUE_PROGRAM_TYPE_NOT_AVAILABLE = 'PROGRAM_TYPE_NOT_AVAILABLE';
    /**
     * Constant for value 'CAMP_PARTICIPATION_CANNOT_BE_CANCELLED'
     * @return string 'CAMP_PARTICIPATION_CANNOT_BE_CANCELLED'
     */
    const VALUE_CAMP_PARTICIPATION_CANNOT_BE_CANCELLED = 'CAMP_PARTICIPATION_CANNOT_BE_CANCELLED';
    /**
     * Constant for value 'CANNOT_PAYOUT_CLIPCARD_INSTALLMENT_PLAN'
     * @return string 'CANNOT_PAYOUT_CLIPCARD_INSTALLMENT_PLAN'
     */
    const VALUE_CANNOT_PAYOUT_CLIPCARD_INSTALLMENT_PLAN = 'CANNOT_PAYOUT_CLIPCARD_INSTALLMENT_PLAN';
    /**
     * Constant for value 'CANCEL_CLIPCARD_SALE_FOR_CAMP_NOT_ALLOWED'
     * @return string 'CANCEL_CLIPCARD_SALE_FOR_CAMP_NOT_ALLOWED'
     */
    const VALUE_CANCEL_CLIPCARD_SALE_FOR_CAMP_NOT_ALLOWED = 'CANCEL_CLIPCARD_SALE_FOR_CAMP_NOT_ALLOWED';
    /**
     * Constant for value 'INSTALLMENTS_FOR_CLIPCARD_WITH_OVERDUE_DEBT_NOT_ALLOWED'
     * @return string 'INSTALLMENTS_FOR_CLIPCARD_WITH_OVERDUE_DEBT_NOT_ALLOWED'
     */
    const VALUE_INSTALLMENTS_FOR_CLIPCARD_WITH_OVERDUE_DEBT_NOT_ALLOWED = 'INSTALLMENTS_FOR_CLIPCARD_WITH_OVERDUE_DEBT_NOT_ALLOWED';
    /**
     * Constant for value 'SUBSCRIPTION_OF_SAME_TYPE_IN_BASKET'
     * @return string 'SUBSCRIPTION_OF_SAME_TYPE_IN_BASKET'
     */
    const VALUE_SUBSCRIPTION_OF_SAME_TYPE_IN_BASKET = 'SUBSCRIPTION_OF_SAME_TYPE_IN_BASKET';
    /**
     * Constant for value 'SHOPPING_BASKET_ITEM_NOT_FOUND'
     * @return string 'SHOPPING_BASKET_ITEM_NOT_FOUND'
     */
    const VALUE_SHOPPING_BASKET_ITEM_NOT_FOUND = 'SHOPPING_BASKET_ITEM_NOT_FOUND';
    /**
     * Constant for value 'SHOPPING_BASKET_WRONG_STATUS'
     * @return string 'SHOPPING_BASKET_WRONG_STATUS'
     */
    const VALUE_SHOPPING_BASKET_WRONG_STATUS = 'SHOPPING_BASKET_WRONG_STATUS';
    /**
     * Constant for value 'SHOPPING_BASKET_NOT_EMPTY'
     * @return string 'SHOPPING_BASKET_NOT_EMPTY'
     */
    const VALUE_SHOPPING_BASKET_NOT_EMPTY = 'SHOPPING_BASKET_NOT_EMPTY';
    /**
     * Constant for value 'SYSTEM_PROPERTY_NOT_SET'
     * @return string 'SYSTEM_PROPERTY_NOT_SET'
     */
    const VALUE_SYSTEM_PROPERTY_NOT_SET = 'SYSTEM_PROPERTY_NOT_SET';
    /**
     * Constant for value 'PERSON_IS_NOT_EMPLOYEE'
     * @return string 'PERSON_IS_NOT_EMPLOYEE'
     */
    const VALUE_PERSON_IS_NOT_EMPLOYEE = 'PERSON_IS_NOT_EMPLOYEE';
    /**
     * Constant for value 'PAYMENT_AGREEMENT_NOT_ALLOWED_FOR_PERSON_TYPE'
     * @return string 'PAYMENT_AGREEMENT_NOT_ALLOWED_FOR_PERSON_TYPE'
     */
    const VALUE_PAYMENT_AGREEMENT_NOT_ALLOWED_FOR_PERSON_TYPE = 'PAYMENT_AGREEMENT_NOT_ALLOWED_FOR_PERSON_TYPE';
    /**
     * Constant for value 'INVALID_NATIONAL_ID'
     * @return string 'INVALID_NATIONAL_ID'
     */
    const VALUE_INVALID_NATIONAL_ID = 'INVALID_NATIONAL_ID';
    /**
     * Constant for value 'INVALID_RESIDENT_ID'
     * @return string 'INVALID_RESIDENT_ID'
     */
    const VALUE_INVALID_RESIDENT_ID = 'INVALID_RESIDENT_ID';
    /**
     * Constant for value 'SEARCH_CRITERIA_EMPTY'
     * @return string 'SEARCH_CRITERIA_EMPTY'
     */
    const VALUE_SEARCH_CRITERIA_EMPTY = 'SEARCH_CRITERIA_EMPTY';
    /**
     * Constant for value 'SUBSCRIPTION_HAS_END_DATE'
     * @return string 'SUBSCRIPTION_HAS_END_DATE'
     */
    const VALUE_SUBSCRIPTION_HAS_END_DATE = 'SUBSCRIPTION_HAS_END_DATE';
    /**
     * Constant for value 'SUBSCRIPTION_IN_WRONG_STATE'
     * @return string 'SUBSCRIPTION_IN_WRONG_STATE'
     */
    const VALUE_SUBSCRIPTION_IN_WRONG_STATE = 'SUBSCRIPTION_IN_WRONG_STATE';
    /**
     * Constant for value 'BOOKING_PROGRAM_PARTICIPATIONS_ARE_TENTATIVE'
     * @return string 'BOOKING_PROGRAM_PARTICIPATIONS_ARE_TENTATIVE'
     */
    const VALUE_BOOKING_PROGRAM_PARTICIPATIONS_ARE_TENTATIVE = 'BOOKING_PROGRAM_PARTICIPATIONS_ARE_TENTATIVE';
    /**
     * Return allowed values
     * @uses self::VALUE_UNKNOWN_ERROR
     * @uses self::VALUE_CREATE_EXCEPTION
     * @uses self::VALUE_REMOVE_EXCEPTION
     * @uses self::VALUE_SECURITY
     * @uses self::VALUE_SECURITY_ROLE
     * @uses self::VALUE_ARGUMENT_IS_NULL
     * @uses self::VALUE_ILLEGAL_ARGUMENT
     * @uses self::VALUE_ILLEGAL_STATE
     * @uses self::VALUE_PARSE_ERROR
     * @uses self::VALUE_ENTITY_NOT_FOUND
     * @uses self::VALUE_CONFLICTING_CHANGE
     * @uses self::VALUE_INVALID_ZIPCODE
     * @uses self::VALUE_INVALID_SOCIAL_SECURITY_NUMBER
     * @uses self::VALUE_INVALID_COMPANYID
     * @uses self::VALUE_INVALID_PHONE_NUMBER
     * @uses self::VALUE_TOO_EARLY
     * @uses self::VALUE_TOO_LATE
     * @uses self::VALUE_CLIENT
     * @uses self::VALUE_ENTITY_IDENTIFIER_STATUS
     * @uses self::VALUE_DUPLICATE_PERSON
     * @uses self::VALUE_DUPLICATE_EXTERNAL_ID
     * @uses self::VALUE_FRIEND_RESTRICTION
     * @uses self::VALUE_EMPLOYEE_ALREADY_EXIST
     * @uses self::VALUE_DUPLICATE_COMPANY
     * @uses self::VALUE_EXPENSIVE_SEARCH
     * @uses self::VALUE_MULTIPLE_PERSONS_FOUND
     * @uses self::VALUE_NOT_DELIVERABLE
     * @uses self::VALUE_CIRCULAR_REFERENCE
     * @uses self::VALUE_REFERENCE
     * @uses self::VALUE_PERSON_NOT_EDITABLE
     * @uses self::VALUE_PERSON_TRANSFER
     * @uses self::VALUE_NO_CLEARING_HOUSE_FOUND
     * @uses self::VALUE_ACCOUNT_RECEIVABLE_IS_BLOCKED
     * @uses self::VALUE_BANK_ACCOUNT_IS_BLOCKED
     * @uses self::VALUE_DEBIT_MAX
     * @uses self::VALUE_INVALID_PAYMENT_AGREEMENT
     * @uses self::VALUE_INVALID_BANK_INFO
     * @uses self::VALUE_NO_CLEARING_HOUSE_CREDITOR_FOUND
     * @uses self::VALUE_PARTICIPATION_TIME_INTERVAL_OVERLAP
     * @uses self::VALUE_PARTICIPATION_PERSON_BLACKLISTED
     * @uses self::VALUE_PARTICIPATION_PERSON_BLOCKED
     * @uses self::VALUE_PARTICIPATION_PERSON_DOUBLE_PARTICIPATION
     * @uses self::VALUE_BOOKING_WAITING_LIST_FULL
     * @uses self::VALUE_BOOKING_OUTSIDE_TIME_INTERVAL
     * @uses self::VALUE_BOOKING_RESTRICTION
     * @uses self::VALUE_OUTSIDE_BOOKING_WINDOW_EXCEPTION
     * @uses self::VALUE_CUSTOMER_CANCEL_OUTSIDE_TIME_INTERVAL
     * @uses self::VALUE_CENTER_CANCEL_OUTSIDE_TIME_INTERVAL
     * @uses self::VALUE_BOOKING_COLLECTION_NOT_PAID_FOR
     * @uses self::VALUE_NO_PRIVILEGE_FOR_PARTICIPATION
     * @uses self::VALUE_ALL_PARTICIPATIONS_TAKEN
     * @uses self::VALUE_SHOWUP_OUTSIDE_TIME_INTERVAL
     * @uses self::VALUE_ILLEGAL_BOOKING_STATE_FOR_CANCELLATION
     * @uses self::VALUE_TOO_EARLY_TO_BOOK_PARTICIPANT
     * @uses self::VALUE_TOO_EARLY_TO_SHOWUP
     * @uses self::VALUE_TOO_LATE_TO_SHOWUP
     * @uses self::VALUE_PARTICIPANT_TOO_MANY_OPEN_BOOKINGS
     * @uses self::VALUE_RESOURCE_OCCUPIED
     * @uses self::VALUE_RESOURCE_START_TIME_RESTRICTION
     * @uses self::VALUE_RESOURCE_TIME_RESTRICTION
     * @uses self::VALUE_ACTIVITY_STATE_INVALID
     * @uses self::VALUE_NO_PARENT_BOOKING_FOUND
     * @uses self::VALUE_STAFF_BLACKLISTED
     * @uses self::VALUE_STAFF_OCCUPIED
     * @uses self::VALUE_TOO_MANY_SUB_STAFF_USAGES
     * @uses self::VALUE_PARTICIPANT_ALREADY_BOOKED
     * @uses self::VALUE_PARTICIPANT_ALREADY_SHOWEDUP
     * @uses self::VALUE_ILLEGAL_BOOKING_STATE_FOR_BOOKING
     * @uses self::VALUE_LISTS_FULL
     * @uses self::VALUE_TOO_LATE_TO_BOOK_PARTICIPANT
     * @uses self::VALUE_PARTICIPANT_OCCUPIED
     * @uses self::VALUE_FULL_BOOKING_RESTRICTION
     * @uses self::VALUE_IN_ADVANCE_LIMIT_BOOKING_RESTRICTION
     * @uses self::VALUE_PERSON_DOUBLE_PARTICIPATION
     * @uses self::VALUE_PARTICIPANT_BLACKLISTED
     * @uses self::VALUE_TOO_LATE_TO_CANCEL_BOOKING
     * @uses self::VALUE_TOO_LATE_TO_CANCEL_BY_CENTER
     * @uses self::VALUE_TOO_LATE_TO_CANCEL_BY_CUSTOMER
     * @uses self::VALUE_UNABLE_TO_REFRESH_WAITING_LIST
     * @uses self::VALUE_TOO_FEW_PARTICIPANTS
     * @uses self::VALUE_FREEZE_MAX_PREBOOK_PERIOD
     * @uses self::VALUE_FREEZE_MIN_OR_MAX_DURATION
     * @uses self::VALUE_FREEZE_TOO_LONG
     * @uses self::VALUE_FREEZE_TOO_SHORT
     * @uses self::VALUE_FREEZE_MAX_NUMBER_REACHED
     * @uses self::VALUE_TOO_MANY_FREEZES_PERIOD
     * @uses self::VALUE_TOO_MANY_FREEZES_TOTAL
     * @uses self::VALUE_FREEZE_MAX_TOTAL_DURATION_REACHED
     * @uses self::VALUE_FREEZE_NOT_ALLOWED
     * @uses self::VALUE_FREEZE_START_AND_STOP_DATE
     * @uses self::VALUE_FREEZE_START_IN_THE_PAST
     * @uses self::VALUE_FREEZE_END_BEFORE_SUBSCRIPTION_END
     * @uses self::VALUE_FREEZE_START_AFTER_SUBSCRIPTION_END
     * @uses self::VALUE_FREEZE_START_BEFORE_SUBSCRIPTION_START
     * @uses self::VALUE_FREEZE_OVERLAP
     * @uses self::VALUE_FREEZE_FUTURE_FREEZE
     * @uses self::VALUE_BOOKINGS_IN_FREEZE_PERIOD
     * @uses self::VALUE_NEGATIVE_SUBSCRIPTION_PRICE
     * @uses self::VALUE_SUBSCRIPTION_INCORRECT_STATE
     * @uses self::VALUE_SUBSCRIPTION_INCORRECT_TYPE
     * @uses self::VALUE_START_DATE_BEFORE_BILLED_UNTIL_DATE
     * @uses self::VALUE_PARTICIPATION_ALREADY_CANCELLED
     * @uses self::VALUE_PARTICIPATION_CANCELLED
     * @uses self::VALUE_MINIMUM_AGE_EXCEPTION
     * @uses self::VALUE_TEMPLATE_NOT_DEFINED
     * @uses self::VALUE_INVALID_OTHER_PAYER_TYPE
     * @uses self::VALUE_DIFFERENT_COUNTRY_FOR_OTHER_PAYER
     * @uses self::VALUE_PERSON_IS_NOT_STAFF
     * @uses self::VALUE_OTHER_PAYER_HAS_OTHER_PAYER
     * @uses self::VALUE_NO_VALID_ACCOUNT_FOR_OTHER_PAYER
     * @uses self::VALUE_PERSON_STATUS_NOT_VALID_FOR_ATTEND
     * @uses self::VALUE_ATTENDING_PERSON_BLACKLISTED
     * @uses self::VALUE_MISSING_PRIVILEGE_FOR_ATTEND
     * @uses self::VALUE_ATTEND_NOT_POSSIBLE_AT_SPECIFIED_TIME
     * @uses self::VALUE_CLIPCARD_NOT_USABLE
     * @uses self::VALUE_TOO_MANY_CLIPS_ON_CLIPCARD
     * @uses self::VALUE_NOT_ENOUGH_CLIPS_ON_CLIPCARD
     * @uses self::VALUE_LOGIN_BLOCKED
     * @uses self::VALUE_PASSWORD_EXPIRED
     * @uses self::VALUE_PERSON_DELETED
     * @uses self::VALUE_PERSON_ANONYMIZED
     * @uses self::VALUE_INVALID_PICTURE
     * @uses self::VALUE_PERSON_NOT_FOUND_FOR_VENDING
     * @uses self::VALUE_TOO_MANY_MATCHES
     * @uses self::VALUE_PASSWORD_NOT_SET
     * @uses self::VALUE_PASSWORD_TOO_SHORT
     * @uses self::VALUE_CANNOT_PARTICIPATE_IN_FREEZE_PERIOD
     * @uses self::VALUE_INVALID_STATUS
     * @uses self::VALUE_CONCURRENT_CALLS
     * @uses self::VALUE_CANNOT_UPDATE_COMPLETED_QUESTIONNAIRE
     * @uses self::VALUE_MISSING_ADMIN_FEE_PRODUCT
     * @uses self::VALUE_LOGIN_NOT_VALID
     * @uses self::VALUE_ACTIVE_TASKS_ALREADY_EXISTS_FOR_PERSON
     * @uses self::VALUE_TASK_TYPE_EXTERNAL_KEY_DOES_NOT_EXIST
     * @uses self::VALUE_SUBSCRIPTION_OF_SAME_TYPE
     * @uses self::VALUE_PERSON_NOT_FOUND
     * @uses self::VALUE_EMPLOYEE_KEY_NOT_FOUND
     * @uses self::VALUE_EMPLOYEE_LOGIN_ALREADY_BLOCKED
     * @uses self::VALUE_EMPLOYEE_LOGIN_ALREADY_UNBLOCKED
     * @uses self::VALUE_INVALID_EMAIL
     * @uses self::VALUE_CENTER_NOT_FOUND
     * @uses self::VALUE_CENTER_ATTRIBUTE_TYPE_NOT_STRING
     * @uses self::VALUE_CENTER_ATTRIBUTE_NOT_FOUND
     * @uses self::VALUE_CENTER_ATTRIBUTE_UPDATE_NOT_ALLOWED
     * @uses self::VALUE_CENTER_ATTRIBUTE_VALUE_NOT_ALLOWED
     * @uses self::VALUE_DEDUCTION_DAY_VALIDATION_FAILED
     * @uses self::VALUE_CHILD_MUST_HAVE_AT_LEAST_ONE_PARENT
     * @uses self::VALUE_CHILD_HAS_TOO_MANY_PARENTS
     * @uses self::VALUE_PARENT_HAS_TOO_MANY_CHILDREN
     * @uses self::VALUE_CHILD_PARENT_RELATIONSHIP_ALREADY_EXISTS
     * @uses self::VALUE_CHILD_PARENT_RELATIONSHIP_DOES_NOT_EXIST
     * @uses self::VALUE_GUARDIAN_IS_TOO_YOUNG
     * @uses self::VALUE_CHILD_IS_TOO_OLD
     * @uses self::VALUE_AGE_NOT_SET
     * @uses self::VALUE_PAYMENT_AGREEMENT_PERSON_MISMATCH
     * @uses self::VALUE_PAYMENT_CYCLE_TYPE_NOT_ALLOWED_EXCEPTION
     * @uses self::VALUE_SUBSCRIPTION_DOES_NOT_BELONG_TO_MEMBER
     * @uses self::VALUE_CANNOT_DELETE_SUBSCRIPTION
     * @uses self::VALUE_NO_PAYMENT_AGREEMENT_FOUND
     * @uses self::VALUE_PAYMENT_AGREEMENT_NOT_VALID_FOR_SALES
     * @uses self::VALUE_PAYMENT_AGREEMENT_NOT_ACTIVE
     * @uses self::VALUE_PAYMENT_CYCLE_NOT_ALLOWED
     * @uses self::VALUE_SUBSCRIPTION_SALE_OUTSIDE_HOME_CLUB_NOT_ALLOWED
     * @uses self::VALUE_BELOW_REQUIRED_AGE_FOR_EMAIL_CAPTURE
     * @uses self::VALUE_ROLE_NOT_FOUND
     * @uses self::VALUE_ACTION_NOT_ALLOWED
     * @uses self::VALUE_SCOPE_NOT_FOUND
     * @uses self::VALUE_EMPLOYEE_BLOCKED
     * @uses self::VALUE_ROLE_NOT_ASSIGNED
     * @uses self::VALUE_INVALID_EMPLOYEE_KEY
     * @uses self::VALUE_MISSING_PRIVILEGES
     * @uses self::VALUE_DATE_PARSE_ERROR
     * @uses self::VALUE_DATE_INVALID
     * @uses self::VALUE_CLIPCARD_NOT_ACTIVE
     * @uses self::VALUE_GROUP_ALREADY_EXIST
     * @uses self::VALUE_EMPLOYEE_AND_PERSON_KEY_SET
     * @uses self::VALUE_MULTIPLE_EMPLOYEE_LOGINS_FOUND
     * @uses self::VALUE_MULTIPLE_BLOCKED_EMPLOYEE_LOGINS_FOUND
     * @uses self::VALUE_NO_ACTIVE_EMPLOYEE_LOGINS_FOUND
     * @uses self::VALUE_NO_BLOCKED_EMPLOYEE_LOGINS_FOUND
     * @uses self::VALUE_INVOICE_LINE_NOT_FOUND
     * @uses self::VALUE_INVALID_CLIP_SIZE
     * @uses self::VALUE_CLIP_SIZE_NOT_SUPPORTED
     * @uses self::VALUE_MISSING_CLIP_SIZE
     * @uses self::VALUE_DUPLICATE_ENTITY_FOUND
     * @uses self::VALUE_RELATION_ALREADY_EXISTS
     * @uses self::VALUE_RELATION_DOESNT_EXIST
     * @uses self::VALUE_FEATURE_NOT_ENABLED
     * @uses self::VALUE_BUYOUT_OVERRIDE_TOO_HIGH
     * @uses self::VALUE_CLIPCARD_NOT_FOUND
     * @uses self::VALUE_SUBSCRIPTION_WRONG_TYPE
     * @uses self::VALUE_INVALID_PAYMENT_CYCLE
     * @uses self::VALUE_INVALID_COURSE_BOOKING
     * @uses self::VALUE_INVALID_COURSE_PRIVILEGE
     * @uses self::VALUE_CLIPCARD_EXPIRED
     * @uses self::VALUE_CLIPCARD_OVERDUE
     * @uses self::VALUE_CLIPCARD_REFUNDED
     * @uses self::VALUE_NOT_WITHIN_AGE_GROUP
     * @uses self::VALUE_TOO_LATE_TO_SIGNUP_TO_COURSE
     * @uses self::VALUE_NOT_A_FIXED_COURSE
     * @uses self::VALUE_ILLEGAL_PAYMENT_AGREEMENT_STATE
     * @uses self::VALUE_MEMBER_CARD_ALREADY_BLOCKED
     * @uses self::VALUE_RECURRING_PARTICIPATION
     * @uses self::VALUE_ACTIVITY_ACCESS_GROUP_NOT_MATCHING
     * @uses self::VALUE_ACTIVITY_RESOURCE_GROUP_NOT_MATCHING
     * @uses self::VALUE_ACTIVITY_CAPACITY_TOO_LOW
     * @uses self::VALUE_ACTIVITY_CAPACITY_NOT_UNLIMITED
     * @uses self::VALUE_NOT_SUPPORTED_FOR_COURSES
     * @uses self::VALUE_BOOKING_PROGRAM_NOT_FOUND
     * @uses self::VALUE_BOOKING_PROGRAM_STARTED
     * @uses self::VALUE_PARTICIPANT_NOT_FOUND_ON_BOOKING_PROGRAM
     * @uses self::VALUE_TOO_LATE_TO_REMOVE_BOOKING_PROGRAM_PARTICIPATION
     * @uses self::VALUE_TOO_EARLY_TO_SIGNUP_PROGRAM_BOOKING
     * @uses self::VALUE_ILLEGAL_DATE_CHANGE
     * @uses self::VALUE_MAXIMUM_CHECKED_IN
     * @uses self::VALUE_RESOURCE_SELECTION_NOT_ALLOW_BY_ACTIVITY
     * @uses self::VALUE_MISSING_RESOURCE
     * @uses self::VALUE_WRONG_INSTALLMENT_PLAN
     * @uses self::VALUE_INVALID_FREE_PERIOD
     * @uses self::VALUE_INVALID_SUBSCRIPTION_STATE
     * @uses self::VALUE_INVALID_SUBSCRIPTION_STATE_AND_SUB_STATE
     * @uses self::VALUE_SUBSCRIPTION_IS_SPONSORED
     * @uses self::VALUE_SUBSCRIPTION_IS_REASSIGNED
     * @uses self::VALUE_CAMP_BOOKING_SELECTION_DATE
     * @uses self::VALUE_CANNOT_CHANGE_OWNER_IN_EXISTING_PARTICIPATIONS
     * @uses self::VALUE_SINGLE_DAYS_BOOKING_NOT_ALLOWED_FOR_CAMP
     * @uses self::VALUE_OWNER_BELOW_MINIMUM_AGE
     * @uses self::VALUE_OVERRIDE_OF_EXPIRATION_DATE_NOT_ALLOWED
     * @uses self::VALUE_MISSING_ISSUE_DATE
     * @uses self::VALUE_CUSTOM_JOURNAL_DOCUMENT_TYPE_NOT_ACTIVE
     * @uses self::VALUE_CUSTOM_JOURNAL_DOCUMENT_MISSING_ATTACHMENT
     * @uses self::VALUE_FAMILY_RELATIVE_RELATIONSHIP_DOES_NOT_EXIST
     * @uses self::VALUE_CAMP_ELECTIVE_SELECTED_WITHOUT_CAMP_BOOKING
     * @uses self::VALUE_PROGRAM_TYPE_NOT_AVAILABLE
     * @uses self::VALUE_CAMP_PARTICIPATION_CANNOT_BE_CANCELLED
     * @uses self::VALUE_CANNOT_PAYOUT_CLIPCARD_INSTALLMENT_PLAN
     * @uses self::VALUE_CANCEL_CLIPCARD_SALE_FOR_CAMP_NOT_ALLOWED
     * @uses self::VALUE_INSTALLMENTS_FOR_CLIPCARD_WITH_OVERDUE_DEBT_NOT_ALLOWED
     * @uses self::VALUE_SUBSCRIPTION_OF_SAME_TYPE_IN_BASKET
     * @uses self::VALUE_SHOPPING_BASKET_ITEM_NOT_FOUND
     * @uses self::VALUE_SHOPPING_BASKET_WRONG_STATUS
     * @uses self::VALUE_SHOPPING_BASKET_NOT_EMPTY
     * @uses self::VALUE_SYSTEM_PROPERTY_NOT_SET
     * @uses self::VALUE_PERSON_IS_NOT_EMPLOYEE
     * @uses self::VALUE_PAYMENT_AGREEMENT_NOT_ALLOWED_FOR_PERSON_TYPE
     * @uses self::VALUE_INVALID_NATIONAL_ID
     * @uses self::VALUE_INVALID_RESIDENT_ID
     * @uses self::VALUE_SEARCH_CRITERIA_EMPTY
     * @uses self::VALUE_SUBSCRIPTION_HAS_END_DATE
     * @uses self::VALUE_SUBSCRIPTION_IN_WRONG_STATE
     * @uses self::VALUE_BOOKING_PROGRAM_PARTICIPATIONS_ARE_TENTATIVE
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_UNKNOWN_ERROR,
            self::VALUE_CREATE_EXCEPTION,
            self::VALUE_REMOVE_EXCEPTION,
            self::VALUE_SECURITY,
            self::VALUE_SECURITY_ROLE,
            self::VALUE_ARGUMENT_IS_NULL,
            self::VALUE_ILLEGAL_ARGUMENT,
            self::VALUE_ILLEGAL_STATE,
            self::VALUE_PARSE_ERROR,
            self::VALUE_ENTITY_NOT_FOUND,
            self::VALUE_CONFLICTING_CHANGE,
            self::VALUE_INVALID_ZIPCODE,
            self::VALUE_INVALID_SOCIAL_SECURITY_NUMBER,
            self::VALUE_INVALID_COMPANYID,
            self::VALUE_INVALID_PHONE_NUMBER,
            self::VALUE_TOO_EARLY,
            self::VALUE_TOO_LATE,
            self::VALUE_CLIENT,
            self::VALUE_ENTITY_IDENTIFIER_STATUS,
            self::VALUE_DUPLICATE_PERSON,
            self::VALUE_DUPLICATE_EXTERNAL_ID,
            self::VALUE_FRIEND_RESTRICTION,
            self::VALUE_EMPLOYEE_ALREADY_EXIST,
            self::VALUE_DUPLICATE_COMPANY,
            self::VALUE_EXPENSIVE_SEARCH,
            self::VALUE_MULTIPLE_PERSONS_FOUND,
            self::VALUE_NOT_DELIVERABLE,
            self::VALUE_CIRCULAR_REFERENCE,
            self::VALUE_REFERENCE,
            self::VALUE_PERSON_NOT_EDITABLE,
            self::VALUE_PERSON_TRANSFER,
            self::VALUE_NO_CLEARING_HOUSE_FOUND,
            self::VALUE_ACCOUNT_RECEIVABLE_IS_BLOCKED,
            self::VALUE_BANK_ACCOUNT_IS_BLOCKED,
            self::VALUE_DEBIT_MAX,
            self::VALUE_INVALID_PAYMENT_AGREEMENT,
            self::VALUE_INVALID_BANK_INFO,
            self::VALUE_NO_CLEARING_HOUSE_CREDITOR_FOUND,
            self::VALUE_PARTICIPATION_TIME_INTERVAL_OVERLAP,
            self::VALUE_PARTICIPATION_PERSON_BLACKLISTED,
            self::VALUE_PARTICIPATION_PERSON_BLOCKED,
            self::VALUE_PARTICIPATION_PERSON_DOUBLE_PARTICIPATION,
            self::VALUE_BOOKING_WAITING_LIST_FULL,
            self::VALUE_BOOKING_OUTSIDE_TIME_INTERVAL,
            self::VALUE_BOOKING_RESTRICTION,
            self::VALUE_OUTSIDE_BOOKING_WINDOW_EXCEPTION,
            self::VALUE_CUSTOMER_CANCEL_OUTSIDE_TIME_INTERVAL,
            self::VALUE_CENTER_CANCEL_OUTSIDE_TIME_INTERVAL,
            self::VALUE_BOOKING_COLLECTION_NOT_PAID_FOR,
            self::VALUE_NO_PRIVILEGE_FOR_PARTICIPATION,
            self::VALUE_ALL_PARTICIPATIONS_TAKEN,
            self::VALUE_SHOWUP_OUTSIDE_TIME_INTERVAL,
            self::VALUE_ILLEGAL_BOOKING_STATE_FOR_CANCELLATION,
            self::VALUE_TOO_EARLY_TO_BOOK_PARTICIPANT,
            self::VALUE_TOO_EARLY_TO_SHOWUP,
            self::VALUE_TOO_LATE_TO_SHOWUP,
            self::VALUE_PARTICIPANT_TOO_MANY_OPEN_BOOKINGS,
            self::VALUE_RESOURCE_OCCUPIED,
            self::VALUE_RESOURCE_START_TIME_RESTRICTION,
            self::VALUE_RESOURCE_TIME_RESTRICTION,
            self::VALUE_ACTIVITY_STATE_INVALID,
            self::VALUE_NO_PARENT_BOOKING_FOUND,
            self::VALUE_STAFF_BLACKLISTED,
            self::VALUE_STAFF_OCCUPIED,
            self::VALUE_TOO_MANY_SUB_STAFF_USAGES,
            self::VALUE_PARTICIPANT_ALREADY_BOOKED,
            self::VALUE_PARTICIPANT_ALREADY_SHOWEDUP,
            self::VALUE_ILLEGAL_BOOKING_STATE_FOR_BOOKING,
            self::VALUE_LISTS_FULL,
            self::VALUE_TOO_LATE_TO_BOOK_PARTICIPANT,
            self::VALUE_PARTICIPANT_OCCUPIED,
            self::VALUE_FULL_BOOKING_RESTRICTION,
            self::VALUE_IN_ADVANCE_LIMIT_BOOKING_RESTRICTION,
            self::VALUE_PERSON_DOUBLE_PARTICIPATION,
            self::VALUE_PARTICIPANT_BLACKLISTED,
            self::VALUE_TOO_LATE_TO_CANCEL_BOOKING,
            self::VALUE_TOO_LATE_TO_CANCEL_BY_CENTER,
            self::VALUE_TOO_LATE_TO_CANCEL_BY_CUSTOMER,
            self::VALUE_UNABLE_TO_REFRESH_WAITING_LIST,
            self::VALUE_TOO_FEW_PARTICIPANTS,
            self::VALUE_FREEZE_MAX_PREBOOK_PERIOD,
            self::VALUE_FREEZE_MIN_OR_MAX_DURATION,
            self::VALUE_FREEZE_TOO_LONG,
            self::VALUE_FREEZE_TOO_SHORT,
            self::VALUE_FREEZE_MAX_NUMBER_REACHED,
            self::VALUE_TOO_MANY_FREEZES_PERIOD,
            self::VALUE_TOO_MANY_FREEZES_TOTAL,
            self::VALUE_FREEZE_MAX_TOTAL_DURATION_REACHED,
            self::VALUE_FREEZE_NOT_ALLOWED,
            self::VALUE_FREEZE_START_AND_STOP_DATE,
            self::VALUE_FREEZE_START_IN_THE_PAST,
            self::VALUE_FREEZE_END_BEFORE_SUBSCRIPTION_END,
            self::VALUE_FREEZE_START_AFTER_SUBSCRIPTION_END,
            self::VALUE_FREEZE_START_BEFORE_SUBSCRIPTION_START,
            self::VALUE_FREEZE_OVERLAP,
            self::VALUE_FREEZE_FUTURE_FREEZE,
            self::VALUE_BOOKINGS_IN_FREEZE_PERIOD,
            self::VALUE_NEGATIVE_SUBSCRIPTION_PRICE,
            self::VALUE_SUBSCRIPTION_INCORRECT_STATE,
            self::VALUE_SUBSCRIPTION_INCORRECT_TYPE,
            self::VALUE_START_DATE_BEFORE_BILLED_UNTIL_DATE,
            self::VALUE_PARTICIPATION_ALREADY_CANCELLED,
            self::VALUE_PARTICIPATION_CANCELLED,
            self::VALUE_MINIMUM_AGE_EXCEPTION,
            self::VALUE_TEMPLATE_NOT_DEFINED,
            self::VALUE_INVALID_OTHER_PAYER_TYPE,
            self::VALUE_DIFFERENT_COUNTRY_FOR_OTHER_PAYER,
            self::VALUE_PERSON_IS_NOT_STAFF,
            self::VALUE_OTHER_PAYER_HAS_OTHER_PAYER,
            self::VALUE_NO_VALID_ACCOUNT_FOR_OTHER_PAYER,
            self::VALUE_PERSON_STATUS_NOT_VALID_FOR_ATTEND,
            self::VALUE_ATTENDING_PERSON_BLACKLISTED,
            self::VALUE_MISSING_PRIVILEGE_FOR_ATTEND,
            self::VALUE_ATTEND_NOT_POSSIBLE_AT_SPECIFIED_TIME,
            self::VALUE_CLIPCARD_NOT_USABLE,
            self::VALUE_TOO_MANY_CLIPS_ON_CLIPCARD,
            self::VALUE_NOT_ENOUGH_CLIPS_ON_CLIPCARD,
            self::VALUE_LOGIN_BLOCKED,
            self::VALUE_PASSWORD_EXPIRED,
            self::VALUE_PERSON_DELETED,
            self::VALUE_PERSON_ANONYMIZED,
            self::VALUE_INVALID_PICTURE,
            self::VALUE_PERSON_NOT_FOUND_FOR_VENDING,
            self::VALUE_TOO_MANY_MATCHES,
            self::VALUE_PASSWORD_NOT_SET,
            self::VALUE_PASSWORD_TOO_SHORT,
            self::VALUE_CANNOT_PARTICIPATE_IN_FREEZE_PERIOD,
            self::VALUE_INVALID_STATUS,
            self::VALUE_CONCURRENT_CALLS,
            self::VALUE_CANNOT_UPDATE_COMPLETED_QUESTIONNAIRE,
            self::VALUE_MISSING_ADMIN_FEE_PRODUCT,
            self::VALUE_LOGIN_NOT_VALID,
            self::VALUE_ACTIVE_TASKS_ALREADY_EXISTS_FOR_PERSON,
            self::VALUE_TASK_TYPE_EXTERNAL_KEY_DOES_NOT_EXIST,
            self::VALUE_SUBSCRIPTION_OF_SAME_TYPE,
            self::VALUE_PERSON_NOT_FOUND,
            self::VALUE_EMPLOYEE_KEY_NOT_FOUND,
            self::VALUE_EMPLOYEE_LOGIN_ALREADY_BLOCKED,
            self::VALUE_EMPLOYEE_LOGIN_ALREADY_UNBLOCKED,
            self::VALUE_INVALID_EMAIL,
            self::VALUE_CENTER_NOT_FOUND,
            self::VALUE_CENTER_ATTRIBUTE_TYPE_NOT_STRING,
            self::VALUE_CENTER_ATTRIBUTE_NOT_FOUND,
            self::VALUE_CENTER_ATTRIBUTE_UPDATE_NOT_ALLOWED,
            self::VALUE_CENTER_ATTRIBUTE_VALUE_NOT_ALLOWED,
            self::VALUE_DEDUCTION_DAY_VALIDATION_FAILED,
            self::VALUE_CHILD_MUST_HAVE_AT_LEAST_ONE_PARENT,
            self::VALUE_CHILD_HAS_TOO_MANY_PARENTS,
            self::VALUE_PARENT_HAS_TOO_MANY_CHILDREN,
            self::VALUE_CHILD_PARENT_RELATIONSHIP_ALREADY_EXISTS,
            self::VALUE_CHILD_PARENT_RELATIONSHIP_DOES_NOT_EXIST,
            self::VALUE_GUARDIAN_IS_TOO_YOUNG,
            self::VALUE_CHILD_IS_TOO_OLD,
            self::VALUE_AGE_NOT_SET,
            self::VALUE_PAYMENT_AGREEMENT_PERSON_MISMATCH,
            self::VALUE_PAYMENT_CYCLE_TYPE_NOT_ALLOWED_EXCEPTION,
            self::VALUE_SUBSCRIPTION_DOES_NOT_BELONG_TO_MEMBER,
            self::VALUE_CANNOT_DELETE_SUBSCRIPTION,
            self::VALUE_NO_PAYMENT_AGREEMENT_FOUND,
            self::VALUE_PAYMENT_AGREEMENT_NOT_VALID_FOR_SALES,
            self::VALUE_PAYMENT_AGREEMENT_NOT_ACTIVE,
            self::VALUE_PAYMENT_CYCLE_NOT_ALLOWED,
            self::VALUE_SUBSCRIPTION_SALE_OUTSIDE_HOME_CLUB_NOT_ALLOWED,
            self::VALUE_BELOW_REQUIRED_AGE_FOR_EMAIL_CAPTURE,
            self::VALUE_ROLE_NOT_FOUND,
            self::VALUE_ACTION_NOT_ALLOWED,
            self::VALUE_SCOPE_NOT_FOUND,
            self::VALUE_EMPLOYEE_BLOCKED,
            self::VALUE_ROLE_NOT_ASSIGNED,
            self::VALUE_INVALID_EMPLOYEE_KEY,
            self::VALUE_MISSING_PRIVILEGES,
            self::VALUE_DATE_PARSE_ERROR,
            self::VALUE_DATE_INVALID,
            self::VALUE_CLIPCARD_NOT_ACTIVE,
            self::VALUE_GROUP_ALREADY_EXIST,
            self::VALUE_EMPLOYEE_AND_PERSON_KEY_SET,
            self::VALUE_MULTIPLE_EMPLOYEE_LOGINS_FOUND,
            self::VALUE_MULTIPLE_BLOCKED_EMPLOYEE_LOGINS_FOUND,
            self::VALUE_NO_ACTIVE_EMPLOYEE_LOGINS_FOUND,
            self::VALUE_NO_BLOCKED_EMPLOYEE_LOGINS_FOUND,
            self::VALUE_INVOICE_LINE_NOT_FOUND,
            self::VALUE_INVALID_CLIP_SIZE,
            self::VALUE_CLIP_SIZE_NOT_SUPPORTED,
            self::VALUE_MISSING_CLIP_SIZE,
            self::VALUE_DUPLICATE_ENTITY_FOUND,
            self::VALUE_RELATION_ALREADY_EXISTS,
            self::VALUE_RELATION_DOESNT_EXIST,
            self::VALUE_FEATURE_NOT_ENABLED,
            self::VALUE_BUYOUT_OVERRIDE_TOO_HIGH,
            self::VALUE_CLIPCARD_NOT_FOUND,
            self::VALUE_SUBSCRIPTION_WRONG_TYPE,
            self::VALUE_INVALID_PAYMENT_CYCLE,
            self::VALUE_INVALID_COURSE_BOOKING,
            self::VALUE_INVALID_COURSE_PRIVILEGE,
            self::VALUE_CLIPCARD_EXPIRED,
            self::VALUE_CLIPCARD_OVERDUE,
            self::VALUE_CLIPCARD_REFUNDED,
            self::VALUE_NOT_WITHIN_AGE_GROUP,
            self::VALUE_TOO_LATE_TO_SIGNUP_TO_COURSE,
            self::VALUE_NOT_A_FIXED_COURSE,
            self::VALUE_ILLEGAL_PAYMENT_AGREEMENT_STATE,
            self::VALUE_MEMBER_CARD_ALREADY_BLOCKED,
            self::VALUE_RECURRING_PARTICIPATION,
            self::VALUE_ACTIVITY_ACCESS_GROUP_NOT_MATCHING,
            self::VALUE_ACTIVITY_RESOURCE_GROUP_NOT_MATCHING,
            self::VALUE_ACTIVITY_CAPACITY_TOO_LOW,
            self::VALUE_ACTIVITY_CAPACITY_NOT_UNLIMITED,
            self::VALUE_NOT_SUPPORTED_FOR_COURSES,
            self::VALUE_BOOKING_PROGRAM_NOT_FOUND,
            self::VALUE_BOOKING_PROGRAM_STARTED,
            self::VALUE_PARTICIPANT_NOT_FOUND_ON_BOOKING_PROGRAM,
            self::VALUE_TOO_LATE_TO_REMOVE_BOOKING_PROGRAM_PARTICIPATION,
            self::VALUE_TOO_EARLY_TO_SIGNUP_PROGRAM_BOOKING,
            self::VALUE_ILLEGAL_DATE_CHANGE,
            self::VALUE_MAXIMUM_CHECKED_IN,
            self::VALUE_RESOURCE_SELECTION_NOT_ALLOW_BY_ACTIVITY,
            self::VALUE_MISSING_RESOURCE,
            self::VALUE_WRONG_INSTALLMENT_PLAN,
            self::VALUE_INVALID_FREE_PERIOD,
            self::VALUE_INVALID_SUBSCRIPTION_STATE,
            self::VALUE_INVALID_SUBSCRIPTION_STATE_AND_SUB_STATE,
            self::VALUE_SUBSCRIPTION_IS_SPONSORED,
            self::VALUE_SUBSCRIPTION_IS_REASSIGNED,
            self::VALUE_CAMP_BOOKING_SELECTION_DATE,
            self::VALUE_CANNOT_CHANGE_OWNER_IN_EXISTING_PARTICIPATIONS,
            self::VALUE_SINGLE_DAYS_BOOKING_NOT_ALLOWED_FOR_CAMP,
            self::VALUE_OWNER_BELOW_MINIMUM_AGE,
            self::VALUE_OVERRIDE_OF_EXPIRATION_DATE_NOT_ALLOWED,
            self::VALUE_MISSING_ISSUE_DATE,
            self::VALUE_CUSTOM_JOURNAL_DOCUMENT_TYPE_NOT_ACTIVE,
            self::VALUE_CUSTOM_JOURNAL_DOCUMENT_MISSING_ATTACHMENT,
            self::VALUE_FAMILY_RELATIVE_RELATIONSHIP_DOES_NOT_EXIST,
            self::VALUE_CAMP_ELECTIVE_SELECTED_WITHOUT_CAMP_BOOKING,
            self::VALUE_PROGRAM_TYPE_NOT_AVAILABLE,
            self::VALUE_CAMP_PARTICIPATION_CANNOT_BE_CANCELLED,
            self::VALUE_CANNOT_PAYOUT_CLIPCARD_INSTALLMENT_PLAN,
            self::VALUE_CANCEL_CLIPCARD_SALE_FOR_CAMP_NOT_ALLOWED,
            self::VALUE_INSTALLMENTS_FOR_CLIPCARD_WITH_OVERDUE_DEBT_NOT_ALLOWED,
            self::VALUE_SUBSCRIPTION_OF_SAME_TYPE_IN_BASKET,
            self::VALUE_SHOPPING_BASKET_ITEM_NOT_FOUND,
            self::VALUE_SHOPPING_BASKET_WRONG_STATUS,
            self::VALUE_SHOPPING_BASKET_NOT_EMPTY,
            self::VALUE_SYSTEM_PROPERTY_NOT_SET,
            self::VALUE_PERSON_IS_NOT_EMPLOYEE,
            self::VALUE_PAYMENT_AGREEMENT_NOT_ALLOWED_FOR_PERSON_TYPE,
            self::VALUE_INVALID_NATIONAL_ID,
            self::VALUE_INVALID_RESIDENT_ID,
            self::VALUE_SEARCH_CRITERIA_EMPTY,
            self::VALUE_SUBSCRIPTION_HAS_END_DATE,
            self::VALUE_SUBSCRIPTION_IN_WRONG_STATE,
            self::VALUE_BOOKING_PROGRAM_PARTICIPATIONS_ARE_TENTATIVE,
        ];
    }
}
