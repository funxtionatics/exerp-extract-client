<?php

declare(strict_types=1);

namespace Extract\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for availableExtractParameterType EnumType
 * @subpackage Enumerations
 */
class AvailableExtractParameterType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'BOOLEAN'
     * @return string 'BOOLEAN'
     */
    const VALUE_BOOLEAN = 'BOOLEAN';
    /**
     * Constant for value 'CENTER'
     * @return string 'CENTER'
     */
    const VALUE_CENTER = 'CENTER';
    /**
     * Constant for value 'DATE'
     * @return string 'DATE'
     */
    const VALUE_DATE = 'DATE';
    /**
     * Constant for value 'NUMBER_ALLOW_NEGATIVE'
     * @return string 'NUMBER_ALLOW_NEGATIVE'
     */
    const VALUE_NUMBER_ALLOW_NEGATIVE = 'NUMBER_ALLOW_NEGATIVE';
    /**
     * Constant for value 'PERCENTAGE'
     * @return string 'PERCENTAGE'
     */
    const VALUE_PERCENTAGE = 'PERCENTAGE';
    /**
     * Constant for value 'NUMBER'
     * @return string 'NUMBER'
     */
    const VALUE_NUMBER = 'NUMBER';
    /**
     * Constant for value 'LONG_DATE'
     * @return string 'LONG_DATE'
     */
    const VALUE_LONG_DATE = 'LONG_DATE';
    /**
     * Constant for value 'PRICE'
     * @return string 'PRICE'
     */
    const VALUE_PRICE = 'PRICE';
    /**
     * Constant for value 'SCOPE'
     * @return string 'SCOPE'
     */
    const VALUE_SCOPE = 'SCOPE';
    /**
     * Constant for value 'TEXT_FIELD'
     * @return string 'TEXT_FIELD'
     */
    const VALUE_TEXT_FIELD = 'TEXT_FIELD';
    /**
     * Constant for value 'LONG_TEXT'
     * @return string 'LONG_TEXT'
     */
    const VALUE_LONG_TEXT = 'LONG_TEXT';
    /**
     * Constant for value 'TEXT'
     * @return string 'TEXT'
     */
    const VALUE_TEXT = 'TEXT';
    /**
     * Constant for value 'GLOBAL_SUBSCRIPTION_TYPES'
     * @return string 'GLOBAL_SUBSCRIPTION_TYPES'
     */
    const VALUE_GLOBAL_SUBSCRIPTION_TYPES = 'GLOBAL_SUBSCRIPTION_TYPES';
    /**
     * Constant for value 'LOCAL_SUBSCRIPTION_TYPES'
     * @return string 'LOCAL_SUBSCRIPTION_TYPES'
     */
    const VALUE_LOCAL_SUBSCRIPTION_TYPES = 'LOCAL_SUBSCRIPTION_TYPES';
    /**
     * Constant for value 'SUBSCRIPTION_STATE'
     * @return string 'SUBSCRIPTION_STATE'
     */
    const VALUE_SUBSCRIPTION_STATE = 'SUBSCRIPTION_STATE';
    /**
     * Constant for value 'PERSONSTATUS_LIST'
     * @return string 'PERSONSTATUS_LIST'
     */
    const VALUE_PERSONSTATUS_LIST = 'PERSONSTATUS_LIST';
    /**
     * Constant for value 'PERSON_TYPE'
     * @return string 'PERSON_TYPE'
     */
    const VALUE_PERSON_TYPE = 'PERSON_TYPE';
    /**
     * Constant for value 'KEY_LIST'
     * @return string 'KEY_LIST'
     */
    const VALUE_KEY_LIST = 'KEY_LIST';
    /**
     * Constant for value 'LIST'
     * @return string 'LIST'
     */
    const VALUE_LIST = 'LIST';
    /**
     * Constant for value 'UQ_LIST'
     * @return string 'UQ_LIST'
     */
    const VALUE_UQ_LIST = 'UQ_LIST';
    /**
     * Constant for value 'MC_LIST'
     * @return string 'MC_LIST'
     */
    const VALUE_MC_LIST = 'MC_LIST';
    /**
     * Constant for value 'INTEGER'
     * @return string 'INTEGER'
     */
    const VALUE_INTEGER = 'INTEGER';
    /**
     * Constant for value 'INTEGER_OPTIONS_MULTIPLE'
     * @return string 'INTEGER_OPTIONS_MULTIPLE'
     */
    const VALUE_INTEGER_OPTIONS_MULTIPLE = 'INTEGER_OPTIONS_MULTIPLE';
    /**
     * Constant for value 'INTEGER_OPTIONS_SINGLE'
     * @return string 'INTEGER_OPTIONS_SINGLE'
     */
    const VALUE_INTEGER_OPTIONS_SINGLE = 'INTEGER_OPTIONS_SINGLE';
    /**
     * Constant for value 'SUBSCRIPTION_ADDON'
     * @return string 'SUBSCRIPTION_ADDON'
     */
    const VALUE_SUBSCRIPTION_ADDON = 'SUBSCRIPTION_ADDON';
    /**
     * Constant for value 'KEY_SINGLE'
     * @return string 'KEY_SINGLE'
     */
    const VALUE_KEY_SINGLE = 'KEY_SINGLE';
    /**
     * Constant for value 'KEY_MULTIPLE'
     * @return string 'KEY_MULTIPLE'
     */
    const VALUE_KEY_MULTIPLE = 'KEY_MULTIPLE';
    /**
     * Constant for value 'STRING_OPTIONS_SINGLE'
     * @return string 'STRING_OPTIONS_SINGLE'
     */
    const VALUE_STRING_OPTIONS_SINGLE = 'STRING_OPTIONS_SINGLE';
    /**
     * Constant for value 'STRING_OPTIONS_MULTIPLE'
     * @return string 'STRING_OPTIONS_MULTIPLE'
     */
    const VALUE_STRING_OPTIONS_MULTIPLE = 'STRING_OPTIONS_MULTIPLE';
    /**
     * Constant for value 'STRING_MULTIPLE'
     * @return string 'STRING_MULTIPLE'
     */
    const VALUE_STRING_MULTIPLE = 'STRING_MULTIPLE';
    /**
     * Constant for value 'LONG_DATE_TIME'
     * @return string 'LONG_DATE_TIME'
     */
    const VALUE_LONG_DATE_TIME = 'LONG_DATE_TIME';
    /**
     * Return allowed values
     * @uses self::VALUE_BOOLEAN
     * @uses self::VALUE_CENTER
     * @uses self::VALUE_DATE
     * @uses self::VALUE_NUMBER_ALLOW_NEGATIVE
     * @uses self::VALUE_PERCENTAGE
     * @uses self::VALUE_NUMBER
     * @uses self::VALUE_LONG_DATE
     * @uses self::VALUE_PRICE
     * @uses self::VALUE_SCOPE
     * @uses self::VALUE_TEXT_FIELD
     * @uses self::VALUE_LONG_TEXT
     * @uses self::VALUE_TEXT
     * @uses self::VALUE_GLOBAL_SUBSCRIPTION_TYPES
     * @uses self::VALUE_LOCAL_SUBSCRIPTION_TYPES
     * @uses self::VALUE_SUBSCRIPTION_STATE
     * @uses self::VALUE_PERSONSTATUS_LIST
     * @uses self::VALUE_PERSON_TYPE
     * @uses self::VALUE_KEY_LIST
     * @uses self::VALUE_LIST
     * @uses self::VALUE_UQ_LIST
     * @uses self::VALUE_MC_LIST
     * @uses self::VALUE_INTEGER
     * @uses self::VALUE_INTEGER_OPTIONS_MULTIPLE
     * @uses self::VALUE_INTEGER_OPTIONS_SINGLE
     * @uses self::VALUE_SUBSCRIPTION_ADDON
     * @uses self::VALUE_KEY_SINGLE
     * @uses self::VALUE_KEY_MULTIPLE
     * @uses self::VALUE_STRING_OPTIONS_SINGLE
     * @uses self::VALUE_STRING_OPTIONS_MULTIPLE
     * @uses self::VALUE_STRING_MULTIPLE
     * @uses self::VALUE_LONG_DATE_TIME
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_BOOLEAN,
            self::VALUE_CENTER,
            self::VALUE_DATE,
            self::VALUE_NUMBER_ALLOW_NEGATIVE,
            self::VALUE_PERCENTAGE,
            self::VALUE_NUMBER,
            self::VALUE_LONG_DATE,
            self::VALUE_PRICE,
            self::VALUE_SCOPE,
            self::VALUE_TEXT_FIELD,
            self::VALUE_LONG_TEXT,
            self::VALUE_TEXT,
            self::VALUE_GLOBAL_SUBSCRIPTION_TYPES,
            self::VALUE_LOCAL_SUBSCRIPTION_TYPES,
            self::VALUE_SUBSCRIPTION_STATE,
            self::VALUE_PERSONSTATUS_LIST,
            self::VALUE_PERSON_TYPE,
            self::VALUE_KEY_LIST,
            self::VALUE_LIST,
            self::VALUE_UQ_LIST,
            self::VALUE_MC_LIST,
            self::VALUE_INTEGER,
            self::VALUE_INTEGER_OPTIONS_MULTIPLE,
            self::VALUE_INTEGER_OPTIONS_SINGLE,
            self::VALUE_SUBSCRIPTION_ADDON,
            self::VALUE_KEY_SINGLE,
            self::VALUE_KEY_MULTIPLE,
            self::VALUE_STRING_OPTIONS_SINGLE,
            self::VALUE_STRING_OPTIONS_MULTIPLE,
            self::VALUE_STRING_MULTIPLE,
            self::VALUE_LONG_DATE_TIME,
        ];
    }
}
