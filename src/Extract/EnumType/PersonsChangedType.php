<?php

declare(strict_types=1);

namespace Extract\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for personsChangedType EnumType
 * @subpackage Enumerations
 */
class PersonsChangedType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'NEW_PERSONS'
     * @return string 'NEW_PERSONS'
     */
    const VALUE_NEW_PERSONS = 'NEW_PERSONS';
    /**
     * Constant for value 'DETAIL_CHANGES'
     * @return string 'DETAIL_CHANGES'
     */
    const VALUE_DETAIL_CHANGES = 'DETAIL_CHANGES';
    /**
     * Constant for value 'STATUS_CHANGES'
     * @return string 'STATUS_CHANGES'
     */
    const VALUE_STATUS_CHANGES = 'STATUS_CHANGES';
    /**
     * Constant for value 'SUBSCRIPTION_STATUS_CHANGES'
     * @return string 'SUBSCRIPTION_STATUS_CHANGES'
     */
    const VALUE_SUBSCRIPTION_STATUS_CHANGES = 'SUBSCRIPTION_STATUS_CHANGES';
    /**
     * Constant for value 'ATTENDS'
     * @return string 'ATTENDS'
     */
    const VALUE_ATTENDS = 'ATTENDS';
    /**
     * Constant for value 'CHECKINS'
     * @return string 'CHECKINS'
     */
    const VALUE_CHECKINS = 'CHECKINS';
    /**
     * Constant for value 'PARTICIPATIONS'
     * @return string 'PARTICIPATIONS'
     */
    const VALUE_PARTICIPATIONS = 'PARTICIPATIONS';
    /**
     * Constant for value 'CLIPCARD_CHANGES'
     * @return string 'CLIPCARD_CHANGES'
     */
    const VALUE_CLIPCARD_CHANGES = 'CLIPCARD_CHANGES';
    /**
     * Return allowed values
     * @uses self::VALUE_NEW_PERSONS
     * @uses self::VALUE_DETAIL_CHANGES
     * @uses self::VALUE_STATUS_CHANGES
     * @uses self::VALUE_SUBSCRIPTION_STATUS_CHANGES
     * @uses self::VALUE_ATTENDS
     * @uses self::VALUE_CHECKINS
     * @uses self::VALUE_PARTICIPATIONS
     * @uses self::VALUE_CLIPCARD_CHANGES
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NEW_PERSONS,
            self::VALUE_DETAIL_CHANGES,
            self::VALUE_STATUS_CHANGES,
            self::VALUE_SUBSCRIPTION_STATUS_CHANGES,
            self::VALUE_ATTENDS,
            self::VALUE_CHECKINS,
            self::VALUE_PARTICIPATIONS,
            self::VALUE_CLIPCARD_CHANGES,
        ];
    }
}
