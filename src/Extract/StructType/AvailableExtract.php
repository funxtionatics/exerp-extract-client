<?php

declare(strict_types=1);

namespace Extract\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for availableExtract StructType
 * @subpackage Structs
 */
class AvailableExtract extends AbstractStructBase
{
    /**
     * The id
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $id = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The parameters
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Extract\StructType\AvailableExtractParameter[]
     */
    protected array $parameters = [];
    /**
     * Constructor method for availableExtract
     * @uses AvailableExtract::setId()
     * @uses AvailableExtract::setName()
     * @uses AvailableExtract::setParameters()
     * @param int $id
     * @param string $name
     * @param \Extract\StructType\AvailableExtractParameter[] $parameters
     */
    public function __construct(?int $id = null, ?string $name = null, array $parameters = [])
    {
        $this
            ->setId($id)
            ->setName($name)
            ->setParameters($parameters);
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \Extract\StructType\AvailableExtract
     */
    public function setId(?int $id = null): self
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Extract\StructType\AvailableExtract
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get parameters value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Extract\StructType\AvailableExtractParameter[]
     */
    public function getParameters(): ?array
    {
        return isset($this->parameters) ? $this->parameters : null;
    }
    /**
     * This method is responsible for validating the values passed to the setParameters method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParameters method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParametersForArrayConstraintsFromSetParameters(array $values = []): string
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $availableExtractParametersItem) {
            // validation for constraint: itemType
            if (!$availableExtractParametersItem instanceof \Extract\StructType\AvailableExtractParameter) {
                $invalidValues[] = is_object($availableExtractParametersItem) ? get_class($availableExtractParametersItem) : sprintf('%s(%s)', gettype($availableExtractParametersItem), var_export($availableExtractParametersItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The parameters property can only contain items of type \Extract\StructType\AvailableExtractParameter, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set parameters value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Extract\StructType\AvailableExtractParameter[] $parameters
     * @return \Extract\StructType\AvailableExtract
     */
    public function setParameters(array $parameters = []): self
    {
        // validation for constraint: array
        if ('' !== ($parametersArrayErrorMessage = self::validateParametersForArrayConstraintsFromSetParameters($parameters))) {
            throw new InvalidArgumentException($parametersArrayErrorMessage, __LINE__);
        }
        if (is_null($parameters) || (is_array($parameters) && empty($parameters))) {
            unset($this->parameters);
        } else {
            $this->parameters = $parameters;
        }
        
        return $this;
    }
    /**
     * Add item to parameters value
     * @throws InvalidArgumentException
     * @param \Extract\StructType\AvailableExtractParameter $item
     * @return \Extract\StructType\AvailableExtract
     */
    public function addToParameters(\Extract\StructType\AvailableExtractParameter $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Extract\StructType\AvailableExtractParameter) {
            throw new InvalidArgumentException(sprintf('The parameters property can only contain items of type \Extract\StructType\AvailableExtractParameter, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->parameters[] = $item;
        
        return $this;
    }
}
