<?php

declare(strict_types=1);

namespace Extract\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for APIException StructType
 * Meta information extracted from the WSDL
 * - type: tns:APIException
 * @subpackage Structs
 */
class APIException extends AbstractStructBase
{
    /**
     * The errorCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $errorCode = null;
    /**
     * The errorDetail
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Extract\StructType\ErrorDetail[]
     */
    protected array $errorDetail = [];
    /**
     * The errorMessage
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $errorMessage = null;
    /**
     * The message
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $message = null;
    /**
     * Constructor method for APIException
     * @uses APIException::setErrorCode()
     * @uses APIException::setErrorDetail()
     * @uses APIException::setErrorMessage()
     * @uses APIException::setMessage()
     * @param string $errorCode
     * @param \Extract\StructType\ErrorDetail[] $errorDetail
     * @param string $errorMessage
     * @param string $message
     */
    public function __construct(?string $errorCode = null, array $errorDetail = [], ?string $errorMessage = null, ?string $message = null)
    {
        $this
            ->setErrorCode($errorCode)
            ->setErrorDetail($errorDetail)
            ->setErrorMessage($errorMessage)
            ->setMessage($message);
    }
    /**
     * Get errorCode value
     * @return string|null
     */
    public function getErrorCode(): ?string
    {
        return $this->errorCode;
    }
    /**
     * Set errorCode value
     * @uses \Extract\EnumType\ErrorCode::valueIsValid()
     * @uses \Extract\EnumType\ErrorCode::getValidValues()
     * @throws InvalidArgumentException
     * @param string $errorCode
     * @return \Extract\StructType\APIException
     */
    public function setErrorCode(?string $errorCode = null): self
    {
        // validation for constraint: enumeration
        if (!\Extract\EnumType\ErrorCode::valueIsValid($errorCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Extract\EnumType\ErrorCode', is_array($errorCode) ? implode(', ', $errorCode) : var_export($errorCode, true), implode(', ', \Extract\EnumType\ErrorCode::getValidValues())), __LINE__);
        }
        $this->errorCode = $errorCode;
        
        return $this;
    }
    /**
     * Get errorDetail value
     * @return \Extract\StructType\ErrorDetail[]
     */
    public function getErrorDetail(): array
    {
        return $this->errorDetail;
    }
    /**
     * This method is responsible for validating the values passed to the setErrorDetail method
     * This method is willingly generated in order to preserve the one-line inline validation within the setErrorDetail method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateErrorDetailForArrayConstraintsFromSetErrorDetail(array $values = []): string
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $aPIExceptionErrorDetailItem) {
            // validation for constraint: itemType
            if (!$aPIExceptionErrorDetailItem instanceof \Extract\StructType\ErrorDetail) {
                $invalidValues[] = is_object($aPIExceptionErrorDetailItem) ? get_class($aPIExceptionErrorDetailItem) : sprintf('%s(%s)', gettype($aPIExceptionErrorDetailItem), var_export($aPIExceptionErrorDetailItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The errorDetail property can only contain items of type \Extract\StructType\ErrorDetail, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set errorDetail value
     * @throws InvalidArgumentException
     * @param \Extract\StructType\ErrorDetail[] $errorDetail
     * @return \Extract\StructType\APIException
     */
    public function setErrorDetail(array $errorDetail = []): self
    {
        // validation for constraint: array
        if ('' !== ($errorDetailArrayErrorMessage = self::validateErrorDetailForArrayConstraintsFromSetErrorDetail($errorDetail))) {
            throw new InvalidArgumentException($errorDetailArrayErrorMessage, __LINE__);
        }
        $this->errorDetail = $errorDetail;
        
        return $this;
    }
    /**
     * Add item to errorDetail value
     * @throws InvalidArgumentException
     * @param \Extract\StructType\ErrorDetail $item
     * @return \Extract\StructType\APIException
     */
    public function addToErrorDetail(\Extract\StructType\ErrorDetail $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Extract\StructType\ErrorDetail) {
            throw new InvalidArgumentException(sprintf('The errorDetail property can only contain items of type \Extract\StructType\ErrorDetail, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->errorDetail[] = $item;
        
        return $this;
    }
    /**
     * Get errorMessage value
     * @return string|null
     */
    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }
    /**
     * Set errorMessage value
     * @param string $errorMessage
     * @return \Extract\StructType\APIException
     */
    public function setErrorMessage(?string $errorMessage = null): self
    {
        // validation for constraint: string
        if (!is_null($errorMessage) && !is_string($errorMessage)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorMessage, true), gettype($errorMessage)), __LINE__);
        }
        $this->errorMessage = $errorMessage;
        
        return $this;
    }
    /**
     * Get message value
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }
    /**
     * Set message value
     * @param string $message
     * @return \Extract\StructType\APIException
     */
    public function setMessage(?string $message = null): self
    {
        // validation for constraint: string
        if (!is_null($message) && !is_string($message)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($message, true), gettype($message)), __LINE__);
        }
        $this->message = $message;
        
        return $this;
    }
}
