<?php

declare(strict_types=1);

namespace Extract\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for apiPersonKey StructType
 * @subpackage Structs
 */
class ApiPersonKey extends CompositeKey
{
    /**
     * The externalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * Constructor method for apiPersonKey
     * @uses ApiPersonKey::setExternalId()
     * @param string $externalId
     */
    public function __construct(?string $externalId = null)
    {
        $this
            ->setExternalId($externalId);
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Extract\StructType\ApiPersonKey
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
}
