<?php

declare(strict_types=1);

namespace Extract\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for extract StructType
 * @subpackage Structs
 */
class Extract extends AbstractStructBase
{
    /**
     * The charsetName
     * @var string|null
     */
    protected ?string $charsetName = null;
    /**
     * The id
     * @var int|null
     */
    protected ?int $id = null;
    /**
     * The parameters
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * @var \Extract\StructType\ExtractParameter[]
     */
    protected array $parameters = [];
    /**
     * Constructor method for extract
     * @uses Extract::setCharsetName()
     * @uses Extract::setId()
     * @uses Extract::setParameters()
     * @param string $charsetName
     * @param int $id
     * @param \Extract\StructType\ExtractParameter[] $parameters
     */
    public function __construct(?string $charsetName = null, ?int $id = null, array $parameters = [])
    {
        $this
            ->setCharsetName($charsetName)
            ->setId($id)
            ->setParameters($parameters);
    }
    /**
     * Get charsetName value
     * @return string|null
     */
    public function getCharsetName(): ?string
    {
        return $this->charsetName;
    }
    /**
     * Set charsetName value
     * @param string $charsetName
     * @return \Extract\StructType\Extract
     */
    public function setCharsetName(?string $charsetName = null): self
    {
        // validation for constraint: string
        if (!is_null($charsetName) && !is_string($charsetName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($charsetName, true), gettype($charsetName)), __LINE__);
        }
        $this->charsetName = $charsetName;
        
        return $this;
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \Extract\StructType\Extract
     */
    public function setId(?int $id = null): self
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        
        return $this;
    }
    /**
     * Get parameters value
     * @return \Extract\StructType\ExtractParameter[]
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }
    /**
     * This method is responsible for validating the values passed to the setParameters method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParameters method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParametersForArrayConstraintsFromSetParameters(array $values = []): string
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $extractParametersItem) {
            // validation for constraint: itemType
            if (!$extractParametersItem instanceof \Extract\StructType\ExtractParameter) {
                $invalidValues[] = is_object($extractParametersItem) ? get_class($extractParametersItem) : sprintf('%s(%s)', gettype($extractParametersItem), var_export($extractParametersItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The parameters property can only contain items of type \Extract\StructType\ExtractParameter, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set parameters value
     * @throws InvalidArgumentException
     * @param \Extract\StructType\ExtractParameter[] $parameters
     * @return \Extract\StructType\Extract
     */
    public function setParameters(array $parameters = []): self
    {
        // validation for constraint: array
        if ('' !== ($parametersArrayErrorMessage = self::validateParametersForArrayConstraintsFromSetParameters($parameters))) {
            throw new InvalidArgumentException($parametersArrayErrorMessage, __LINE__);
        }
        $this->parameters = $parameters;
        
        return $this;
    }
    /**
     * Add item to parameters value
     * @throws InvalidArgumentException
     * @param \Extract\StructType\ExtractParameter $item
     * @return \Extract\StructType\Extract
     */
    public function addToParameters(\Extract\StructType\ExtractParameter $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Extract\StructType\ExtractParameter) {
            throw new InvalidArgumentException(sprintf('The parameters property can only contain items of type \Extract\StructType\ExtractParameter, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->parameters[] = $item;
        
        return $this;
    }
}
