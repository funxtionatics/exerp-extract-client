<?php

declare(strict_types=1);

namespace Extract\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for errorDetail StructType
 * @subpackage Structs
 */
class ErrorDetail extends AbstractStructBase
{
    /**
     * The info
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $info = null;
    /**
     * The type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for errorDetail
     * @uses ErrorDetail::setInfo()
     * @uses ErrorDetail::setType()
     * @param string $info
     * @param string $type
     */
    public function __construct(?string $info = null, ?string $type = null)
    {
        $this
            ->setInfo($info)
            ->setType($type);
    }
    /**
     * Get info value
     * @return string|null
     */
    public function getInfo(): ?string
    {
        return $this->info;
    }
    /**
     * Set info value
     * @param string $info
     * @return \Extract\StructType\ErrorDetail
     */
    public function setInfo(?string $info = null): self
    {
        // validation for constraint: string
        if (!is_null($info) && !is_string($info)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($info, true), gettype($info)), __LINE__);
        }
        $this->info = $info;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @uses \Extract\EnumType\ErrorDetailType::valueIsValid()
     * @uses \Extract\EnumType\ErrorDetailType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $type
     * @return \Extract\StructType\ErrorDetail
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: enumeration
        if (!\Extract\EnumType\ErrorDetailType::valueIsValid($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Extract\EnumType\ErrorDetailType', is_array($type) ? implode(', ', $type) : var_export($type, true), implode(', ', \Extract\EnumType\ErrorDetailType::getValidValues())), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
