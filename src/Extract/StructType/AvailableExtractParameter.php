<?php

declare(strict_types=1);

namespace Extract\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for availableExtractParameter StructType
 * @subpackage Structs
 */
class AvailableExtractParameter extends AbstractStructBase
{
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for availableExtractParameter
     * @uses AvailableExtractParameter::setName()
     * @uses AvailableExtractParameter::setType()
     * @param string $name
     * @param string $type
     */
    public function __construct(?string $name = null, ?string $type = null)
    {
        $this
            ->setName($name)
            ->setType($type);
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Extract\StructType\AvailableExtractParameter
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @uses \Extract\EnumType\AvailableExtractParameterType::valueIsValid()
     * @uses \Extract\EnumType\AvailableExtractParameterType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $type
     * @return \Extract\StructType\AvailableExtractParameter
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: enumeration
        if (!\Extract\EnumType\AvailableExtractParameterType::valueIsValid($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Extract\EnumType\AvailableExtractParameterType', is_array($type) ? implode(', ', $type) : var_export($type, true), implode(', ', \Extract\EnumType\AvailableExtractParameterType::getValidValues())), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
