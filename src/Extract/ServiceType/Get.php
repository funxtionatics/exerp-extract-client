<?php

declare(strict_types=1);

namespace Extract\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Get ServiceType
 * @subpackage Services
 */
class Get extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named getAvailableExtracts
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @return \Extract\ArrayType\AvailableExtractArray|bool
     */
    public function getAvailableExtracts()
    {
        try {
            $this->setResult($resultGetAvailableExtracts = $this->getSoapClient()->__soapCall('getAvailableExtracts', [], [], [], $this->outputHeaders));
        
            return $resultGetAvailableExtracts;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getChangedPersons
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $center
     * @param string $fromDate
     * @param string $toDate
     * @param string $changeType
     * @return \Extract\ArrayType\ApiPersonKeyArray|bool
     */
    public function getChangedPersons($center, $fromDate, $toDate, $changeType)
    {
        try {
            $this->setResult($resultGetChangedPersons = $this->getSoapClient()->__soapCall('getChangedPersons', [
                $center,
                $fromDate,
                $toDate,
                $changeType,
            ], [], [], $this->outputHeaders));
        
            return $resultGetChangedPersons;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Extract\ArrayType\ApiPersonKeyArray|\Extract\ArrayType\AvailableExtractArray
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
