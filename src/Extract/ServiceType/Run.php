<?php

declare(strict_types=1);

namespace Extract\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Run ServiceType
 * @subpackage Services
 */
class Run extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named runExtract
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Extract\StructType\Extract $extract
     * @return \Extract\StructType\MimeDocument|bool
     */
    public function runExtract(\Extract\StructType\Extract $extract)
    {
        try {
            $this->setResult($resultRunExtract = $this->getSoapClient()->__soapCall('runExtract', [
                $extract,
            ], [], [], $this->outputHeaders));
        
            return $resultRunExtract;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Extract\StructType\MimeDocument
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
