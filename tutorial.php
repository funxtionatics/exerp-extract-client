<?php
/**
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the first needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientBase class (each generated ServiceType class extends this class)
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = [
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://goodlife-test.exerp.com/api/v5/ExtractAPI?wsdl',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * ];
 * etc...
 */
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = [
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://goodlife-test.exerp.com/api/v5/ExtractAPI?wsdl',
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \Extract\ClassMap::get(),
];
/**
 * Samples for Get ServiceType
 */
$get = new \Extract\ServiceType\Get($options);
/**
 * Sample call for getAvailableExtracts operation/method
 */
if ($get->getAvailableExtracts() !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getChangedPersons operation/method
 */
if ($get->getChangedPersons($center, $fromDate, $toDate, $changeType) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Samples for Run ServiceType
 */
$run = new \Extract\ServiceType\Run($options);
/**
 * Sample call for runExtract operation/method
 */
if ($run->runExtract(new \Extract\StructType\Extract()) !== false) {
    print_r($run->getResult());
} else {
    print_r($run->getLastError());
}
